const express = require("express");
const app = express();
const pdfMake = require("pdfmake/build/pdfmake");
const vfsFonts = require("pdfmake/build/vfs_fonts");
pdfMake.vfs = vfsFonts.pdfMake.vfs;
const fs = require("fs");
const moment = require("moment");
const { ChartJSNodeCanvas } = require("chartjs-node-canvas");

const gravities = ["Légère", "Mineure", "Moyenne", "Grave"];

const player = {
  club_slug: "FUS Rabat",
  club_id: 1831,
  nom: "Maouhoub",
  prenom: "El Mehdi",
  date_naiss: "2003-06-05",
  club: "FUS Rabat",
  post: "A",
  pied: "droitier",
  nationalite_ids: [],
  equipe_name: "MAR U20 (M)",
  equipe_slug: "MAR U20 (M)",
  equipe_categorie: "U21",
  equipe_logo: "http://localhost:3000/uploads/image-1647350674365.png",
  equipe_sexe: "m",
  equipe_genre: "football",
  equipe_id: 597,
  img: "http://localhost:3000/uploads/image-1652805738264.png",
  but: 0,
  cartonJaune: 0,
  cartonBleu: 0,
  cartonRouge: 0,
  matchJoue: 4,
  weight: "12.73",
  height: "10",
  nationalites: "",
  freres: [],
  tactiques: [
    {
      id: 2907,
      fermerEspace: 0,
      pressing: 0,
      replacement: 0,
      pertinenceJeu: 0,
      creativite: 0,
      intelligence: 0,
      user_id: 231,
      joueur_id: 2232,
      saison_id: 21,
      date: null,
      sub_id: null,
    },
  ],
  playerTiming: {
    id: 2232,
    user_id: 231,
    nom: "Maouhoub",
    prenom: "El Mehdi",
    adresse: "",
    email: "",
    tel_p: "(+212) 6 41 93 85 78",
    tel_fix: "",
    contact_defaut: null,
    img: "http://localhost:3000/uploads/image-1652805738264.png",
    post: "A",
    capitaine: "non",
    num: "14",
    taille_maillot: "",
    certif_med: "",
    ex_club: "",
    ex_club_date: null,
    parc_scol: "",
    second_post: "",
    num_licence: "",
    pointur: "",
    taille_short: "",
    licence: "ok",
    parc_sport: "",
    allergies: null,
    hospital: "RAS",
    contre_indica_medic: "RAS",
    date_naiss: "2003-06-05",
    lieu_naiss: "Rabat",
    nationalite: "MAROCAINE",
    pied: "droitier",
    activted: null,
    equipe_id: 597,
    cni: "",
    passport: "",
    cni_expiration: null,
    passport_expiration: null,
    cni_image: null,
    passport_image: null,
    autorisation_sortie: null,
    formulaire_inscription: null,
    formulaire_licence: null,
    questionnaire_medical: null,
    accord_parentel: null,
    carte_securite_social: null,
    certificat_securite_social: null,
    carnet_sante: null,
    autorisation_utilisation_image: null,
    hebergement: null,
    ecole: null,
    cotisation: null,
    contrat: null,
    justificat_domicile: null,
    dernier_bulletin: null,
    rib_number: null,
    rib: null,
    engagement_financier: null,
    date_club: null,
    etat_lieu_entrant: null,
    autorisation_responsabilte_medical: null,
    etat_lieu_sortant: null,
    reglements_interieur: null,
    club: "FUS Rabat",
    type: "national",
    status: "inapte_maladie",
    notes: null,
    need_glasses: 0,
    vision_type: "",
    strength_glasses_near: 0,
    strength_glasses_middle: 0,
    match_risk: null,
    qualification: "Professionnel",
    adresse_etrange: "",
    pere: "",
    fonction_pere: "",
    mere: "",
    mere_fonction: "",
    contact_urgence: "",
    contact_urgence_tele: "",
    nomber_freres: "0",
    token: null,
    device_type: null,
    base_img: "null",
    sub_user: null,
    sexe: "m",
    date_debut_pret: null,
    date_return_pret: null,
    salaire_pret: null,
    club_pret: null,
    instat_name: "",
    gps_type: "",
    gps_id: "",
    status_pret: "player",
    description: null,
    preparateur: "Hassan Loudari",
    tel_preparateur: "(+212) 6 74 91 07 37",
    email_preparateur: "hassaneloudari2@hotmail.fr",
    post_athlete: "A",
    nationalite_id: 111,
    club_id: 1831,
    nationalite_ids: null,
    pere_nationalite_ids: null,
    mere_nationalite_ids: null,
    scout_source: null,
    scout_temps_jeu: null,
    autre_scout_source: null,
    deleted_user_id: null,
    deleted_at: null,
    contact_urgence_tele_code: 149,
    tel_p_code: 149,
    titulaire: 4,
    nontitulaire: 0,
    but: 0,
    passe: 0,
    arret_decisif: 0,
    minutes: 306,
    nombreMatch: 21,
  },
  techniques: [
    {
      id: 2935,
      dribble: 0,
      corner: 0,
      coupFranc: 0,
      controle: 0,
      marquage: 0,
      tete: 0,
      passe: 0,
      jeuTete: 0,
      fiabilite: 0,
      elimination: 0,
      defensive: 0,
      finition: 0,
      user_id: 231,
      joueur_id: 2232,
      saison_id: 21,
      date: "2021-09-02 21:53:19",
      sub_id: null,
    },
  ],
  trainings: [
    {
      date_at: "2022-03-22",
      id: 59733,
      joueur_id: 2232,
      entrainement_id: 3704,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Mental,Technique",
      theme: "Collaboration,Communication,Passing court",
    },
    {
      date_at: "2022-03-22",
      id: 59773,
      joueur_id: 2232,
      entrainement_id: 3705,
      user_id: 231,
      status: "blessé",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique",
      theme: "Principes offensif,Transition défensif",
    },
    {
      date_at: "2022-03-23",
      id: 59834,
      joueur_id: 2232,
      entrainement_id: 3707,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique",
      theme: "Pressing,Principes défensif",
    },
    {
      date_at: "2022-03-23",
      id: 59874,
      joueur_id: 2232,
      entrainement_id: 3708,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Phases",
      theme: " PA offensif,PA défensif",
    },
    {
      date_at: "2022-03-25",
      id: 59914,
      joueur_id: 2232,
      entrainement_id: 3709,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Physique",
      theme: " Mobilité/stabilité/assouplissement ,Renforcement Haut ",
    },
    {
      date_at: "2022-03-26",
      id: 59954,
      joueur_id: 2232,
      entrainement_id: 3710,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique",
      theme: "Intra line défensive,Transition défensif",
    },
    {
      date_at: "2022-03-26",
      id: 59995,
      joueur_id: 2232,
      entrainement_id: 3711,
      user_id: 231,
      status: "exempté",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique",
      theme: "Automatisme offensif",
    },
    {
      date_at: "2022-03-27",
      id: 60036,
      joueur_id: 2232,
      entrainement_id: 3712,
      user_id: 231,
      status: "exempté",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Mental,Technique",
      theme: "Contrôle des émotions,Finition,Persévérance",
    },
    {
      date_at: "2022-03-27",
      id: 60077,
      joueur_id: 2232,
      entrainement_id: 3713,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Mental,Technique",
      theme: "Contrôle des émotions,Finition,Persévérance",
    },
    {
      date_at: "2022-03-28",
      id: 60118,
      joueur_id: 2232,
      entrainement_id: 3714,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique,Phases",
      theme: " PA offensif,PA défensif,Tactique défensif",
    },
    {
      date_at: "2022-03-29",
      id: 60260,
      joueur_id: 2232,
      entrainement_id: 3719,
      user_id: 231,
      status: "exempté",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Technique",
      theme: "Technique général",
    },
    {
      date_at: "2022-03-21",
      id: 60301,
      joueur_id: 2232,
      entrainement_id: 3720,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Physique",
      theme:
        " Mobilité/stabilité/assouplissement , SOUPLESSE,Gainage ,Proprioception ",
    },
    {
      date_at: "2022-10-10",
      id: 66107,
      joueur_id: 2232,
      entrainement_id: 4173,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique,Technique",
      theme: "Principes offensif,Technique général",
    },
    {
      date_at: "2022-10-11",
      id: 66130,
      joueur_id: 2232,
      entrainement_id: 4174,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique,Technique",
      theme: "Automatisme offensif,Details par poste,Finition",
    },
    {
      date_at: "2022-10-11",
      id: 66153,
      joueur_id: 2232,
      entrainement_id: 4175,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique,Technique",
      theme: "Automatisme offensif,Centres,Details par poste,Finition",
    },
    {
      date_at: "2022-10-12",
      id: 66176,
      joueur_id: 2232,
      entrainement_id: 4176,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique,Phases",
      theme: "Automatisme offensif,Transition offensif",
    },
    {
      date_at: "2022-10-13",
      id: 66199,
      joueur_id: 2232,
      entrainement_id: 4177,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique,Physique",
      theme: "Principes défensif,Principes offensif",
    },
    {
      date_at: "2022-10-15",
      id: 66455,
      joueur_id: 2232,
      entrainement_id: 4189,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique",
      theme: "Automatisme offensif,Principes défensif",
    },
    {
      date_at: "2022-10-16",
      id: 66478,
      joueur_id: 2232,
      entrainement_id: 4190,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique,Phases",
      theme: "PA défensif,Pressing",
    },
    {
      date_at: "2022-10-17",
      id: 66501,
      joueur_id: 2232,
      entrainement_id: 4191,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Physique,Technique,Phases",
      theme: " PA offensif,Finition,Vitesse",
    },
    {
      date_at: "2022-10-19",
      id: 66524,
      joueur_id: 2232,
      entrainement_id: 4192,
      user_id: 231,
      status: "exempté",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: "Séance de récupération et soins à l'hôtel",
      tags: "Physique,Technique",
      theme: "Centres,Finition,Hautes intensités ",
    },
    {
      date_at: "2022-10-20",
      id: 66547,
      joueur_id: 2232,
      entrainement_id: 4193,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique,Physique,Phases",
      theme: " PA offensif,Tactique défensif,Vitesse",
    },
    {
      date_at: "2022-10-22",
      id: 66570,
      joueur_id: 2232,
      entrainement_id: 4194,
      user_id: 231,
      status: "exempté",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: "Séance de récupération et soins à l'hôtel",
      tags: "Physique,Technique",
      theme: "Finition,Hautes intensités ",
    },
    {
      date_at: "2022-10-23",
      id: 66576,
      joueur_id: 2232,
      entrainement_id: 4195,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: "Tactique,Physique,Phases",
      theme: " PA offensif,Principes défensif,Vitesse",
    },
    {
      date_at: "2023-02-01",
      id: 70191,
      joueur_id: 2232,
      entrainement_id: 4321,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: null,
      theme: null,
    },
    {
      date_at: "2023-02-06",
      id: 70303,
      joueur_id: 2232,
      entrainement_id: 4324,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: null,
      theme: null,
    },
    {
      date_at: "2023-02-14",
      id: 70323,
      joueur_id: 2232,
      entrainement_id: 4332,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: null,
      theme: null,
    },
    {
      date_at: "2023-02-14",
      id: 70433,
      joueur_id: 2232,
      entrainement_id: 4333,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: null,
      theme: null,
    },
    {
      date_at: "2024-02-07",
      id: 70549,
      joueur_id: 2232,
      entrainement_id: 4337,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: null,
      theme: null,
    },
    {
      date_at: "2024-02-07",
      id: 70659,
      joueur_id: 2232,
      entrainement_id: 4338,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: null,
      theme: null,
    },
    {
      date_at: "2024-02-07",
      id: 70769,
      joueur_id: 2232,
      entrainement_id: 4339,
      user_id: 231,
      status: "présent",
      grade: 5,
      equipe_id: 597,
      saison_id: 21,
      comment: null,
      tags: null,
      theme: null,
    },
  ],
  athletiques: [
    {
      id: 2875,
      equilibre: 0,
      nombreAcceleration: 0,
      agilite: 0,
      qualitePhyNat: 0,
      detente: 0,
      puissance: 0,
      vitesse: 0,
      elasticite: 0,
      explosivite: 0,
      endurance: 0,
      user_id: 231,
      joueur_id: 2232,
      saison_id: 21,
      date: null,
      sub_id: null,
    },
  ],
  matchs: [
    {
      adversaire_name: "Roumanie\t",
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      id: 15196,
      user_id: 231,
      adversaire: "705",
      competition: "209",
      match_date: "2022-03-29",
      heure: "16:0",
      out_in: "in",
      dure: "",
      stade: "166",
      adress: "Rabat Salé",
      saison_id: 21,
      activated: null,
      arbitre: "KECHCHAF MUSTAPHA (MAROC)",
      conditions_meteo: "humide / pluie",
      adver_systeme_jeu: "4411",
      adver_point_faible:
        "Pas à l'aise enjeu possession court. Défense qui ne défend pas en avançant, beaucoup d'espace dans le dos de leur ligne médiane",
      adver_point_fort:
        "Très intense dans les courses et beaucoup d'impact physique. Un jeu de possession direct et long. Recherche le duel et présent sur les deuxièmes ballon",
      note_match:
        "Face à une équipe plus âgé et plus athlétique.\nNous avons encaissé un but évitable en première période sur leur seul occasion, pour le reste nous avons eu la possession mais trop stérile. Cela manquait de verticalité et de profondeur.\nEn deuxième période Khalifi nous a apporté du calmes de la fluidité et de la verticalité dans nos phases de progression\nLe repositionnement de RADID à gauche et l'entrée de BOUHOUCH à droite nous ont apporté la profondeur qu'il nous manquait en première période",
      terrain: "stabilisé",
      equipe_id: 597,
      otherOpponentName: "Roumanie",
      consignes:
        "B-: \nBloc médian ( ne pas se faire aspirer car il sont performant dans le jeu long.\nGérer l'espace dans le dos de notre défense et replis pour gagner les deuxièmes ballons\nB+ :\nDisponibilité dans le dos de leur ligne médiane puis attaquer l'espace dans le dos de la défense.\nBasculer le jeu rapidement pour attaquer côté faible de l'adversaire.\nTransition défensive :\nGarder l'équilibre lors de nos attaque avec une supériorité ( +1)",
      audio: "null",
      sub_user: null,
      adver_nbr_jeu: 11,
      deleted_at: null,
      trash_id: null,
      minutes: 55,
      joueur_id: 2232,
      match_id: 2789,
      note: 2,
      convoquer: 0,
      equipe_convoquer: null,
      but: 0,
      assists: 0,
      cartonJaune: 0,
      cartonRouge: 0,
      arrets: 0,
      rapports: [
        "Joue pour lui. Toujours en décalage avec le porteur de balle ( beaucoup de hors-jeu). Pas assez varié dans ses appels. A souvent fait le mauvais choix avec le ballon. Pas assez positif dans sa communication avec ses partenaires.",
      ],
    },
    {
      adversaire_name: "Tunisie\t",
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      id: 16199,
      user_id: 231,
      adversaire: "695",
      competition: "211",
      match_date: "2022-10-18",
      heure: "12:0",
      out_in: "out",
      dure: "",
      stade: null,
      adress: "SUEZ ( Egypte)",
      saison_id: 21,
      activated: null,
      arbitre: "Omar Abdulkadir ",
      conditions_meteo: "chaleur",
      adver_systeme_jeu: "433",
      adver_point_faible:
        "Lenteur de la défense, Des joueurs médians athlétiques mais qui ont du mal sous pression. Bloc très compact qui laisse des espaces sur les côtés lors des renversements.",
      adver_point_fort:
        "Organisation et rigueur défensive. Bloc compact . Joueurs athlétique et fort dans les duels. Performant lors des transitions offensives et sur les coups de pieds arrêtés off et déf.",
      note_match:
        "Nous avons réalisé un très bon match dans l'ensemble. Il n'a manqué que les buts. Nous avons eu le contrôle et avons su contourner leur bloc à plusieurs reprises. Défensivement, il n'on eu qu'une seul possibilité avec un bon pressing. Nous leur avons pas donné la possibilité d'être performant en transitions off.\nMalgré tout notre manque de qualité et de détermination dans les les  derniers mètres nous a empêcher de remporter le match",
      terrain: "pelouse",
      equipe_id: 597,
      otherOpponentName: "TUNISIE U20",
      consignes:
        "Contourner le bloc puis attaquer le dos des défenseurs axiaux\nRenversement du jeu rapide\nOff marking lors de nos attaques\n\n",
      audio: "null",
      sub_user: null,
      adver_nbr_jeu: 11,
      deleted_at: "2023-02-23T15:29:28.000Z",
      trash_id: 231,
      minutes: 79,
      joueur_id: 2232,
      match_id: 2869,
      note: 2,
      convoquer: 0,
      equipe_convoquer: null,
      but: 0,
      assists: 0,
      cartonJaune: 0,
      cartonRouge: 0,
      arrets: 0,
      rapports: [
        "A raté deux grosses occasions! Pour le reste, il a souvent fait le mauvais choix et pas assez intelligent dans ses déplacements.",
      ],
    },
    {
      adversaire_name: "Algérie",
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      id: 16211,
      user_id: 231,
      adversaire: "685",
      competition: "211",
      match_date: "2022-10-21",
      heure: "15:0",
      out_in: "out",
      dure: "",
      stade: "STADE DE SUEZ",
      adress: "SUEZ ( Egypte)",
      saison_id: 21,
      activated: null,
      arbitre: "Mahmoud Elbana",
      conditions_meteo: "vent",
      adver_systeme_jeu: "343",
      adver_point_faible:
        "Manque de rigueur défensive. Laisse beaucoup d'espaces à la perte du ballon. Trop de gestes techniques inutiles",
      adver_point_fort:
        "Quelques individualités et de l'enthousiasme lors de leurs attaques. Bonne maitrise collective du ballon.",
      note_match:
        "Après un bon début de match en maitrise mais sans pouvoir trouver les espaces dans leurs dos. Nous marquons sur coup franc, puis plus rien. Nous déjouons complètement. On pense à perdre du temps et casser le rythme au lieu de resté calme et de se tenir au plan",
      terrain: "pelouse",
      equipe_id: 597,
      otherOpponentName: "ALGERIE U20",
      consignes:
        "Chercher à casser des lignes pour perforer le bloc et/ou renverser rapidement le jeu pour créer des 2 contre 1 sur les flancs.\nProfité de nos force sur coups de pieds arrêter offensifs",
      audio: "null",
      sub_user: 308,
      adver_nbr_jeu: 11,
      deleted_at: "2023-02-23T15:29:19.000Z",
      trash_id: 231,
      minutes: 82,
      joueur_id: 2232,
      match_id: 2870,
      note: 2,
      convoquer: 0,
      equipe_convoquer: null,
      but: 0,
      assists: 0,
      cartonJaune: 0,
      cartonRouge: 0,
      arrets: 0,
      rapports: [
        "Pas assez présent devant. Trop de précipitations que ce soit dans ses prises de décisions ou dans ses appels de balle",
      ],
    },
    {
      adversaire_name: "LIBYE ",
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      id: 16230,
      user_id: 231,
      adversaire: "686",
      competition: "211",
      match_date: "2022-10-24",
      heure: "12:0",
      out_in: "out",
      dure: "",
      stade: null,
      adress: "SUEZ ( Egypte)",
      saison_id: 21,
      activated: null,
      arbitre: "Youssef JEMMI",
      conditions_meteo: "vent",
      adver_systeme_jeu: "343",
      adver_point_faible:
        "Peut d'élaboration du jeu. Joue long systématiquement",
      adver_point_fort: "bloc bas et duel aérien. transition rapide.",
      note_match:
        "Une première mi-temps ou nous avons eu les opportunités pour marquer. En deuxième, nous nous sommes trop précipité et avons manqué de lucidité et de justesse dans les passes clés.\n",
      terrain: "pelouse",
      equipe_id: 597,
      otherOpponentName: "LIBYE U20",
      consignes:
        "nous avons insisté sur le contre press à la perte et la gestion des longs ballons. En possession, nous avons demandé à contourner le bloc et centrer.",
      audio: "null",
      sub_user: null,
      adver_nbr_jeu: 11,
      deleted_at: "2023-02-23T14:58:13.000Z",
      trash_id: 231,
      minutes: 90,
      joueur_id: 2232,
      match_id: 2871,
      note: 2,
      convoquer: 0,
      equipe_convoquer: null,
      but: 0,
      assists: 0,
      cartonJaune: 0,
      cartonRouge: 0,
      arrets: 0,
      rapports: [
        "Il n'a rien réussit tout le match. Des mauvais choix de la précipitation et une perte de balle qui provoque le but adverse",
      ],
    },
  ],
  mentales: [
    {
      id: 2892,
      anticipation: 0,
      courage: 0,
      concentration: 0,
      decision: 0,
      jeuSansBall: 0,
      agressivite: 0,
      competiteur: 0,
      charisme: 0,
      leader: 0,
      combativite: null,
      genereux: 0,
      determine: 0,
      user_id: 231,
      joueur_id: 2232,
      saison_id: 21,
      date: null,
      sub_id: null,
    },
  ],
  squat_jump: 0,
  cmj: 0,
  biometrie: [
    {
      poids: "12.73",
      taille: "185.00",
      year: 2023,
      month: 1,
    },
    {
      poids: "72.50",
      taille: "185.00",
      year: 2022,
      month: 3,
    },
    {
      poids: "72.00",
      taille: "185.00",
      year: 2021,
      month: 1,
    },
  ],
  comments: [],
  entrainements: [
    {
      type: 0,
      rpe_estime: 7,
      commentaire: "",
      comment: null,
      entrainement_id: 1276,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "en retard",
      grade: 3,
      date: "2020-12-23 8:0",
      duree: 45,
      tags: [],
      themes: ["PHYSIQUE"],
    },
    {
      type: 0,
      rpe_estime: 0,
      commentaire: "",
      comment: null,
      entrainement_id: 1265,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2021-01-03 7:33",
      duree: 60,
      tags: [],
      themes: ["APRES MEDI"],
    },
    {
      type: 0,
      rpe_estime: 0,
      commentaire: "",
      comment: null,
      entrainement_id: 1243,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2021-01-11 8:0",
      duree: 0,
      tags: [],
      themes: [" Entraînement matinal "],
    },
    {
      type: 0,
      rpe_estime: 0,
      commentaire: "",
      comment: null,
      entrainement_id: 1275,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2021-01-21 7:30",
      duree: 30,
      tags: [],
      themes: ["MATIN VENDREDI"],
    },
    {
      type: 0,
      rpe_estime: 6,
      commentaire: "",
      comment: null,
      entrainement_id: 1404,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "en retard",
      grade: 3,
      date: "2021-02-27 18:0",
      duree: 45,
      tags: [],
      themes: ["Technique"],
    },
    {
      type: 0,
      rpe_estime: 2,
      commentaire: "",
      comment: null,
      entrainement_id: 2315,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2021-05-21 15:43",
      duree: 60,
      tags: [],
      themes: ["Technique mental"],
    },
    {
      type: 0,
      rpe_estime: 2,
      commentaire: "",
      comment: null,
      entrainement_id: 3720,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-03-21 06:30",
      duree: 90,
      tags: [],
      themes: [
        " Mobilité/stabilité/assouplissement ",
        " SOUPLESSE",
        "Gainage ",
        "Proprioception ",
      ],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire:
        "Disponibilité dans le phase de progression, se déplacer en fonction de ses partenaires direct",
      comment: null,
      entrainement_id: 3705,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "blessé",
      grade: 5,
      date: "2022-03-22 10:30",
      duree: 90,
      tags: [],
      themes: ["Principes offensif", "Transition défensif"],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire:
        "Séance de reprise avec comme objectif un travail de conservation collective avec réaction et harcèlement à la perte du ballon",
      comment: null,
      entrainement_id: 3704,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-03-22 16:30",
      duree: 90,
      tags: [],
      themes: ["Collaboration", "Communication", "Passing court"],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire: "Hauteur du bloc et zône de pression . ",
      comment: null,
      entrainement_id: 3707,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-03-23 10:30",
      duree: 90,
      tags: [],
      themes: ["Pressing", "Principes défensif"],
    },
    {
      type: 0,
      rpe_estime: 1,
      commentaire: "Mise en place à vide",
      comment: null,
      entrainement_id: 3708,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-03-23 15:30",
      duree: 90,
      tags: [],
      themes: [" PA offensif", "PA défensif"],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire:
        "Groupe 1 récupération. Groupe 2: Renforcement haut du corps",
      comment: null,
      entrainement_id: 3709,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-03-25 10:30",
      duree: 90,
      tags: [],
      themes: [" Mobilité/stabilité/assouplissement ", "Renforcement Haut "],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire: "",
      comment: null,
      entrainement_id: 3710,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-03-26 10:30",
      duree: 90,
      tags: [],
      themes: ["Intra line défensive", "Transition défensif"],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire: "",
      comment: null,
      entrainement_id: 3711,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "exempté",
      grade: 5,
      date: "2022-03-26 16:30",
      duree: 90,
      tags: [],
      themes: ["Automatisme offensif"],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire:
        "travail de finition pour évaluer la qualité de frappe et de centres de chaque joueurs. Forme de match avec élimination pour évaluer leadership et persévérence",
      comment: null,
      entrainement_id: 3712,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "exempté",
      grade: 5,
      date: "2022-03-27 10:30",
      duree: 90,
      tags: [],
      themes: ["Contrôle des émotions", "Finition", "Persévérance"],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire:
        "Travail de la finition pour évaluer les qualités de frappes et de centres de chaque joueur. Forme de match avec élimination pour évaluer le leadership et la perséverence",
      comment: null,
      entrainement_id: 3713,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-03-27 16:30",
      duree: 90,
      tags: [],
      themes: ["Contrôle des émotions", "Finition", "Persévérance"],
    },
    {
      type: 0,
      rpe_estime: 2,
      commentaire:
        "Travail de vivacité avant match + PA pour les titulaires. 10 v 10 pour les autres",
      comment: null,
      entrainement_id: 3714,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-03-28 15:0",
      duree: 90,
      tags: [],
      themes: [" PA offensif", "PA défensif", "Tactique défensif"],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire: "Entrainement sur la vision et réactivité",
      comment: null,
      entrainement_id: 3719,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "exempté",
      grade: 5,
      date: "2022-03-29 10:30",
      duree: 60,
      tags: [],
      themes: ["Technique général"],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire:
        "Attaque pas les flancs. centres ( early), automatismes ( winger/poket)",
      comment: null,
      entrainement_id: 4173,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-10-10 17:30",
      duree: 90,
      tags: [],
      themes: ["Principes offensif", "Technique général"],
    },
    {
      type: 0,
      rpe_estime: 4,
      commentaire: "",
      comment: null,
      entrainement_id: 4174,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-10-11 10:30",
      duree: 90,
      tags: [],
      themes: ["Automatisme offensif", "Details par poste", "Finition"],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire:
        "Automatisme dans un 1-3-4-3, renversement du jeu, centres et occupation de la surface",
      comment: null,
      entrainement_id: 4175,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-10-11 17:30",
      duree: 90,
      tags: [],
      themes: [
        "Automatisme offensif",
        "Centres",
        "Details par poste",
        "Finition",
      ],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire:
        "Automatisme en phase de progression ( renversement + infiltration entre Déf cent et Latéral adverse). Transition offensive",
      comment: null,
      entrainement_id: 4176,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-10-12 17:0",
      duree: 120,
      tags: [],
      themes: ["Automatisme offensif", "Transition offensif"],
    },
    {
      type: 0,
      rpe_estime: 4,
      commentaire: "Match global 10 + G / 10 = G",
      comment: null,
      entrainement_id: 4177,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-10-13 10:30",
      duree: 90,
      tags: [],
      themes: ["Principes défensif", "Principes offensif"],
    },
    {
      type: 0,
      rpe_estime: 5,
      commentaire:
        "Glissement, couverture, alignement, pour l'échelon défensif et médians déf. Finition après infiltration axial avec et sans ballon pour les joueurs off ",
      comment: null,
      entrainement_id: 4189,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-10-15 15:00",
      duree: 90,
      tags: [],
      themes: ["Automatisme offensif", "Principes défensif"],
    },
    {
      type: 0,
      rpe_estime: 4,
      commentaire:
        "Plan de jeu défensif + toutes  les coups de pied arrêtés défensif",
      comment: null,
      entrainement_id: 4190,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-10-16 14:30",
      duree: 90,
      tags: [],
      themes: ["PA défensif", "Pressing"],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire:
        "Finition sur transition + stratégie coups de pied arrêtés offensifs",
      comment: null,
      entrainement_id: 4191,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-10-17 14:30",
      duree: 75,
      tags: [],
      themes: [" PA offensif", "Finition", "Vitesse"],
    },
    {
      type: 0,
      rpe_estime: 5,
      commentaire:
        "Séance pour les non titulaires. Finition sur renversement et centre",
      comment: "Séance de récupération et soins à l'hôtel",
      entrainement_id: 4192,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "exempté",
      grade: 5,
      date: "2022-10-19 14:00",
      duree: 90,
      tags: [],
      themes: ["Centres", "Finition", "Hautes intensités "],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire: "",
      comment: null,
      entrainement_id: 4193,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-10-20 14:00",
      duree: 75,
      tags: [],
      themes: [" PA offensif", "Tactique défensif", "Vitesse"],
    },
    {
      type: 0,
      rpe_estime: 5,
      commentaire:
        "Séance d'après match avec les non-titulaires. Frappe à distance et infiltration sans ballon dans l'axe",
      comment: "Séance de récupération et soins à l'hôtel",
      entrainement_id: 4194,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "exempté",
      grade: 5,
      date: "2022-10-22 14:00",
      duree: 90,
      tags: [],
      themes: ["Finition", "Hautes intensités "],
    },
    {
      type: 0,
      rpe_estime: 3,
      commentaire:
        "Vivacité, automatisme défensif pour défendre sur les longs ballons. Stratégie sur corners et coups francs offensif",
      comment: null,
      entrainement_id: 4195,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2022-10-23 15:30",
      duree: 75,
      tags: [],
      themes: [" PA offensif", "Principes défensif", "Vitesse"],
    },
    {
      type: 0,
      rpe_estime: 0,
      commentaire: "HSJSG",
      comment: null,
      entrainement_id: 4321,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2023-02-01 5:56",
      duree: 180,
      tags: ["Physique", "Technique"],
      themes: [
        'HI (40")',
        'Passes (30")',
        'Passes (30")',
        'Échauffement (0")',
        'Échauffement (10")',
        'Échauffement avec ballon (30")',
      ],
    },
    {
      type: 1,
      rpe_estime: 0,
      commentaire: "TEST",
      comment: null,
      entrainement_id: 4324,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2023-02-06 18:27",
      duree: 40,
      tags: ["Physique"],
      themes: [
        'HI (30")',
        'Échauffement avec ballon (20")',
        'Échauffement sans ballon (80")',
        'Force (0")',
        'Force spécifique (0")',
      ],
    },
    {
      type: 0,
      rpe_estime: 0,
      commentaire: "SLKQLKS",
      comment: null,
      entrainement_id: 4332,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2023-02-14 22:45",
      duree: 0,
      tags: ["Physique"],
      themes: [
        'Échauffement avec ballon (90")',
        'Force (77")',
        'Échauffement (0")',
        'Échauffement avec ballon (0")',
      ],
    },
    {
      type: 0,
      rpe_estime: 0,
      commentaire: "SLKQLKS",
      comment: null,
      entrainement_id: 4333,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2023-02-14 22:45",
      duree: 0,
      tags: ["Physique"],
      themes: [
        'Échauffement avec ballon (90")',
        'Force (77")',
        'Échauffement (0")',
        'Échauffement avec ballon (0")',
      ],
    },
    {
      type: 0,
      rpe_estime: 0,
      commentaire: "SJHJS",
      comment: null,
      entrainement_id: 4337,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2024-02-07 17:39",
      duree: 0,
      tags: ["Physique"],
      themes: [
        'HI (9")',
        'Échauffement (100")',
        'Échauffement avec ballon (0")',
      ],
    },
    {
      type: 0,
      rpe_estime: 0,
      commentaire: "SJHJS",
      comment: null,
      entrainement_id: 4339,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2024-02-07 17:39",
      duree: 0,
      tags: ["Physique"],
      themes: [
        'Échauffement (100")',
        'Échauffement avec ballon (0")',
        'HI (0")',
      ],
    },
    {
      type: 0,
      rpe_estime: 0,
      commentaire: "SJHJS",
      comment: null,
      entrainement_id: 4338,
      stage_date_fin: "2039-01-01",
      stage_date_debut: "2016-01-01",
      stage_title: "TEST09",
      status: "présent",
      grade: 5,
      date: "2024-02-07 18:39",
      duree: 0,
      tags: ["Physique"],
      themes: [
        'Échauffement (100")',
        'Échauffement avec ballon (0")',
        'HI (0")',
      ],
    },
  ],
  capacitie_physique: [
    {
      type: "graisseuse",
      valeur: "999999",
    },
    {
      type: "labo",
      valeur: "205",
    },
    {
      type: "repos",
      valeur: "199",
    },
  ],
  stage: {
    id: 130,
    joueur_ids: "2578,2696,3379,3215,3198,3358,2232",
    date_debut: "2016-01-01",
    date_fin: "2039-01-01",
    titre: "TEST09",
    user_id: 231,
    equipe_id: 597,
    status: 1,
    created_at: "2023-02-13T11:53:36.000Z",
    deleted_at: null,
    trash_id: null,
    type_id: null,
    lieu: null,
    languages: null,
    location_id: null,
    staffs_ids: null,
    closed_at: null,
  },
  gps: [
    {
      rpe: 1,
      title: "Match 24-10-2022 12:00",
      nbr_desc: 0,
      id: 2554,
      joueur_id: 2232,
      option_id: 2871,
      type: "match",
      user_id: 231,
      equipe_id: 597,
      saison_id: 21,
      gps_field_id: 35,
      category_id: 0,
      gps: [
        {
          name: "duration",
          gps_id: 35,
          unit: "mm:ss",
          color: "#279648",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "25:15",
        },
        {
          name: "distance",
          gps_id: 28,
          unit: "m",
          color: "#b450a2",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "2994.8",
        },
        {
          name: "distance / sp Z2",
          gps_id: 27,
          unit: "m",
          color: "#259827",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "465.9",
        },
        {
          name: "distance / sp Z3",
          gps_id: 29,
          unit: "m",
          color: "#e21d90",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "283.8",
        },
        {
          name: "distance / sp Z4",
          gps_id: 30,
          unit: "m",
          color: "#d77d28",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "122.2",
        },
        {
          name: "speed events",
          gps_id: 31,
          unit: "km/h",
          color: "#a74444",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "9",
        },
        {
          name: "max speed",
          gps_id: 32,
          unit: "km/h",
          color: "#4b39a2",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "31.32",
        },
        {
          name: "acc events",
          gps_id: 33,
          unit: "",
          color: "#1bd1de",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "15",
        },
        {
          name: "dec events",
          gps_id: 34,
          unit: "",
          color: "#0f14ae",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "17",
        },
      ],
    },
    {
      rpe: 1,
      title: "Entrainement 08-01-2023 05:00",
      nbr_desc: 2,
      id: 2157,
      joueur_id: 2232,
      option_id: 4294,
      type: "entrainement",
      user_id: 231,
      equipe_id: 682,
      saison_id: 21,
      gps_field_id: 27,
      category_id: 0,
      gps: [
        {
          name: "distance / sp Z2",
          gps_id: 27,
          unit: "m",
          color: "#259827",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "200.5",
        },
        {
          name: "distance / sp Z3",
          gps_id: 29,
          unit: "m",
          color: "#e21d90",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "71.5",
        },
        {
          name: "distance / sp Z4",
          gps_id: 30,
          unit: "m",
          color: "#d77d28",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "10.5",
        },
        {
          name: "max speed",
          gps_id: 32,
          unit: "km/h",
          color: "#4b39a2",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "28.11",
        },
        {
          name: "acc events",
          gps_id: 33,
          unit: "",
          color: "#1bd1de",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "27",
        },
        {
          name: "dec events",
          gps_id: 34,
          unit: "",
          color: "#0f14ae",
          type_gps: "0, 1, 2, 3",
          category: "1, 2",
          value: "25",
        },
      ],
    },
  ],
  note: 4,
  labelsTags: ["Physique", "Technique", "HI"],
  countTags: {
    Physique: 0,
    Technique: 0,
    HI: 0,
  },
  blessures: [
    {
      date_blessure: "19-01-2023",
      type_blessure: "déchirure",
      localisation: "cuisse gauche",
      gravity: 1,
      date_retour_prevue: null,
    },
    {
      date_blessure: "31-01-2023",
      type_blessure: "déchirure",
      localisation: "poitrine",
      gravity: 1,
      date_retour_prevue: null,
    },
  ],
};

function getDaysAbsence(value = null) {
  const array = [
    { value: 0, label: "0 Jours" },
    { value: 1, label: "1 Jour" },
    { value: 2, label: "2 Jours" },
    { value: 7, label: "1 Semaine" },
    { value: 14, label: "2 Semaines" },
    { value: 30, label: "plus de 4 semaines" },
  ];
  return value == null ? array : array.find((e) => e.value == value)?.label;
}

function getPostPosition(post) {
  let position = { x: -200, y: -200 };
  switch (post) {
    // ! Avant centre droit
    case "ALD":
      position = { x: 490, y: 175 };
      break;
    // ! Avant centre gauche
    case "ALG":
      position = { x: 490, y: 110 };
      break;
    // ! Gardien
    case "G":
      position = { x: 435,
        y: 145};
      break;
    // ! Défenseur
    case "D":
      position = { x: 451, y: 160 };
      break;
    // ! Défenseur droit
    case "DD":
      position = { x: 450,
        y: 175};
      break;
    // ! Défenseur gauche
    case "DG":
      position = { x: 450,
        y: 110 };
      break;
    // ! Défenseur central
    case "DC":
      position = {x: 450,
        y: 145 };
      break;
    // ! ATTackant droit
    case "AD":
      position = {x: 540,
        y: 175 };
      break;
    // ! attackant gauche
    case "AG":
      position = { x: 540,
        y: 110 };
      break;
    // ! Milieu
    case "M":
      position = {x: 500,
        y: 145 };
      break;
    // ! Milieu droit
    case "MDR":
      position = { x: 500,
        y: 160};
      break;
    //!  Milieu gauche
    case "MG":
      position = { x: 500,
        y: 130 };
      break;
    // ! Attaquant
    case "A":
      position = {
        x: 550,
        y: 145
      }
      break;
    // ! Attaquant axial
    case "AA":
      position = { x: 550,
        y: 155};
      break;

    default:
      break;
  }
  return position;
}

const imc = (taille, poid) => {
  if (
    [Number(taille), Number(poid)].includes(NaN) ||
    Number(taille) < 1 ||
    Number(poid) < 1
  )
    return 0;
  let val = String(poid / ((Number(taille) / 100) * (Number(taille) / 100)));
  return val.substring(0, 2) + "," + val.substring(2, 4);
};

const getRpeEstime = (index) => {
  var rpeEstimes = (intensite = [
    {
      index: "0",
      value: " ",
    },
    {
      index: "1",
      value: "1 - Très léger",
    },
    {
      index: "2",
      value: "2 - Leger",
    },
    {
      index: "3",
      value: "3 - Modéré",
    },
    {
      index: "4",
      value: "4 - Légèrement dur",
    },
    {
      index: "5",
      value: "5 - Dur",
    },
    {
      index: "6",
      value: "6 - Dur",
    },
    {
      index: "7",
      value: "7 - Très dur",
    },
    {
      index: "8",
      value: "8 - Très Très dur",
    },
    {
      index: "9",
      value: "9 - trés trés dur",
    },
    {
      index: "10",
      value: "10 - Similaire à ma compétition la plus pénible",
    },
  ]);
  var new_post = rpeEstimes.find((e) => String(e.index) == String(index));
  return [null, undefined, "null"].includes(new_post) ? "" : new_post.value;
};

const getPost = (post) => {
  var posts = {
    G: "Gardien",
    D: "Défenseur",
    AD: "Arrière droit",
    DC: "Défenseur central",
    AG: "Arrière gauche",
    M: "Milieu",
    MDR: "Milieu droit",
    MA: "Milieu axial",
    MG: "Milieu gauche",
    MO: "Milieu offensif",
    MDF: "Milieu défensif",
    A: "Attaquant",
    ALD: "Ailier droit",
    AA: "Attaquant axial",
    ALG: "Ailier gauche",
  };
  var new_post = null;
  Object.keys(posts).forEach((key) => {
    if (key == post) {
      new_post = posts[key];
    }
  });
  return new_post == null ? post : new_post;
};

const getMonth = (nbr) => {
  return [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Août",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décember",
  ][nbr - 1];
};

function createChart(type, data, labels = null) {
  let chartObj,
    width = 800,
    height = 600;
  switch (type) {
    case "techniques":
      let techniques = data.length > 0 ? data[0] : {};
      let dataTechniques =
        data.length > 0
          ? [
              techniques.dribble,
              techniques.corner,
              techniques.coupFranc,
              techniques.controle,
              techniques.marquage,
              techniques.tete,
              techniques.passe,
              techniques.jeuTete,
              techniques.fiabilite,
              techniques.elimination,
              techniques.defensive,
              techniques.finition,
            ]
          : Array(12).fill(0);
      chartObj = {
        type: "radar",
        data: {
          labels: [
            "dribbles",
            "corners",
            "coups francs",
            "controles",
            "marquage",
            "tête",
            "passes",
            "jeu de tête",
            "fiabilite",
            "elimination",
            "defensive",
            "finition",
          ],
          datasets: [
            {
              label: "Techniques",
              backgroundColor: "rgb(8, 160, 103)",
              data: dataTechniques,
            },
          ],
        },
        options: {
          legend: {
            labels: {
              usePointStyle: true,
            },
          },
          scale: {
            ticks: {
              beginAtZero: true,
              max: 10,
              min: 0,
              stepSize: 1,
            },
          },
          cutoutPercentage: 70,
          responsive: true,
          maintainAspectRatio: true,
        },
      };
      break;
    case "tactiques":
      let tactiques = data.length > 0 ? data[0] : {};
      let dataTactiques =
        data.length > 0
          ? [
              tactiques.fermerEspace,
              tactiques.pressing,
              tactiques.replacement,
              tactiques.pertinenceJeu,
              tactiques.creativite,
              tactiques.intelligence,
            ]
          : Array(6).fill(0);

      chartObj = {
        type: "radar",
        data: {
          labels: [
            "fermer les espaces",
            "pressing",
            "replacement",
            "pertinence de jeu",
            "creativite",
            "intelligence de jeu",
          ],
          datasets: [
            {
              label: "Tactiques",
              backgroundColor: "rgb(8, 160, 103)",
              data: dataTactiques,
            },
          ],
        },
        options: {
          legend: {
            labels: {
              usePointStyle: true,
            },
          },
          scale: {
            ticks: {
              beginAtZero: true,
              max: 10,
              min: 0,
              stepSize: 1,
            },
          },
          cutoutPercentage: 70,
          responsive: true,
          maintainAspectRatio: true,
        },
      };
      break;
    case "athletiques":
      let athletiques = data.length > 0 ? data[0] : {};
      let dataAthletiques =
        data.length > 0
          ? [
              athletiques.equilibre,
              athletiques.nombreAcceleration,
              athletiques.agilite,
              athletiques.qualitePhyNat,
              athletiques.detente,
              athletiques.puissance,
              athletiques.vitesse,
              athletiques.elasticite,
              athletiques.explosivite,
              athletiques.endurance,
            ]
          : Array(10).fill(0);
      chartObj = {
        type: "radar",
        data: {
          labels: [
            "equilibre",
            "nombre d'accélerations",
            "agilite",
            "qualité Phy. Nat.",
            "détente",
            "puissance",
            "vitesse",
            "elasticite",
            "explosivite",
            "endurance",
          ],
          datasets: [
            {
              label: "Athletiques",
              backgroundColor: "rgb(8, 160, 103)",
              data: dataAthletiques,
            },
          ],
        },
        options: {
          legend: {
            labels: {
              usePointStyle: true,
            },
          },
          scale: {
            ticks: {
              beginAtZero: true,
              max: 10,
              min: 0,
              stepSize: 1,
            },
          },
          cutoutPercentage: 70,
          responsive: true,
          maintainAspectRatio: true,
        },
      };
      break;

    case "mentales":
      let mentales = data.length > 0 ? data[0] : {};
      let dataMentales =
        data.length > 0
          ? [
              mentales.anticipation,
              mentales.courage,
              mentales.concentration,
              mentales.decision,
              mentales.jeuSansBall,
              mentales.agressivite,
              mentales.competiteur,
              mentales.charisme,
              mentales.leader,
              mentales.combativite,
              mentales.genereux,
              mentales.determine,
            ]
          : Array(12).fill(0);
      chartObj = {
        type: "radar",
        data: {
          labels: [
            "anticipation",
            "courage",
            "concentration",
            "decision",
            "jeu sans ball.",
            "agressivite",
            "competiteur",
            "charisme",
            "leader",
            "combativite",
            "genereux",
            "determine",
          ],
          datasets: [
            {
              label: "Mentales",
              backgroundColor: "rgb(8, 160, 103)",
              data: dataMentales,
            },
          ],
        },
        options: {
          legend: {
            labels: {
              usePointStyle: true,
            },
          },
          scale: {
            ticks: {
              beginAtZero: true,
              max: 10,
              min: 0,
              stepSize: 1,
            },
          },
          cutoutPercentage: 70,
          responsive: true,
          maintainAspectRatio: true,
        },
      };
      break;
    case "tags":
      var countTags = data;
      width = 700;
      height = 500;
      var dataTags =
        Object.keys(countTags).length > 0
          ? labels.map((label) => countTags[label])
          : Array(5).fill(0);
      chartObj = {
        type: "pie",
        data: {
          labels: labels,
          datasets: [
            {
              label: "Tags",
              backgroundColor: [
                "#A6B7EB",
                "#3cba9f",
                "#ff6162",
                "#B13B5C",
                "#a9fB5C",
              ].slice(0, labels.length),
              data: dataTags,
            },
          ],
        },
        options: {
          plugins: {
            datalabels: {
              color: "#2c2f33",
              textAlign: "center",
              borderRadius: 50,
              backgroundColor: "#f8f8f8",
              padding: dataTags.map((n) =>
                [0, null, "", "null"].includes(n)
                  ? 0
                  : Object({
                      left: 12,
                      right: 12,
                      top: 5,
                      bottom: 5,
                    })
              ),
              font: {
                size: 20,
              },
              // ctx.chart.data.labels[ctx.dataIndex] + '\n' +
              formatter: function (value, ctx) {
                return value == 0 ? "" : value;
              },
            },
          },
        },
      };
      break;
    case "gps-total":
      chartObj = {
        type: "bar",
        data: {
          labels: ["", ""],
          datasets: [
            {
              label: "",
              data: [data[1], data[0]],
              backgroundColor: ["#5da64e", "#c9371c"],
            },
          ],
        },
        options: {
          layout: {
            padding: {
              left: 0,
              right: 0,
              top: 65,
              bottom: 0,
            },
          },
          scale: {
            pointLabels: {
              fontSize: 80,
            },
          },
          plugins: {
            legend: {
              display: false,
            },
            datalabels: {
              color: "#fff",
              textAlign: "center",
              // borderWidth: 2,
              // borderColor: '#fff',
              // borderRadius: 5,
              padding: {
                left: 12,
                right: 12,
                top: 5,
                bottom: 5,
              },
              // anchor:'end',
              // align:'top',
              font: {
                size: 30,
              },
              formatter: function (value, ctx) {
                var arr = ["Entrainements", "Matchs"];
                return value == 0 ? "" : value + " min\n" + arr[ctx.dataIndex];
              },
            },
          },
        },
      };
      break;
    case "gps-training":
      chartObj = {
        type: "bar",
        data: {
          labels: data.labels,
          datasets: [
            {
              label: "apples",
              data: data.values,
              backgroundColor: "#5da64e",
            },
          ],
        },
        options: {
          layout: {
            padding: {
              left: 0,
              right: 0,
              top: 35,
              bottom: 0,
            },
          },
          plugins: {
            legend: {
              display: false,
            },
            datalabels: {
              color: "#000",
              textAlign: "center",
              font: {
                size: 30,
              },
              anchor: "end",
              align: "top",
              // ctx.chart.data.labels[ctx.dataIndex] + '\n' +
              formatter: function (value, ctx) {
                return value == 0 ? "" : value;
              },
            },
          },
        },
      };
      break;
    case "entrainements":
      width = 700;
      height = 500;
      var countEntrainements = data;
      var dataEntrainements =
        Object.keys(countEntrainements).length > 0
          ? [
              countEntrainements["présent"],
              countEntrainements["absent"],
              countEntrainements["pas impliqué"],
              countEntrainements["exclu"],
              countEntrainements["en retard"],
              countEntrainements["blessé"],
              countEntrainements["exempté"],
              countEntrainements["devoir international"],
              countEntrainements["malade"],
            ]
          : Array(9).fill(0);

      chartObj = {
        type: "pie",
        data: {
          labels: [
            "Présent",
            "Absent",
            "Pas impliqué",
            "Exclu",
            "En retard",
            "Blessé",
            "Exempté",
            "Devoir international",
            "Malade",
          ],
          datasets: [
            {
              label: "Entrainements",
              backgroundColor: [
                "#3e95cd",
                "#8e5ea2",
                "##f104d4",
                "#ff6162",
                "#17daad",
                "#c10202",
                "#f9b906",
                "#00a023",
                "#3cba9f",
              ],
              data: dataEntrainements,
            },
          ],
        },
        options: {
          plugins: {
            datalabels: {
              color: "#2c2f33",
              textAlign: "center",
              borderRadius: 50,
              backgroundColor: dataEntrainements.map((n) =>
                [0, null, "", "null", "0", undefined, "undefined"].includes(n)
                  ? ""
                  : "#f8f8f8"
              ),
              padding: dataEntrainements.map((n) =>
                [0, null, "", "null", "0", undefined, "undefined"].includes(n)
                  ? 0
                  : Object({
                      left: 12,
                      right: 12,
                      top: 5,
                      bottom: 5,
                    })
              ),
              font: {
                size: 20,
              },
              // ctx.chart.data.labels[ctx.dataIndex] + '\n' +
              formatter: function (value, ctx) {
                return value == 0 ? "" : value;
              },
            },
          },
        },
      };
      break;
    default:
      break;
  }
  return new Promise(function (resolve, reject) {
    try {
      const chartJSNodeCanvas = new ChartJSNodeCanvas({
        width: width,
        height: height,
        plugins: {
          requireLegacy: ["chartjs-plugin-datalabels"],
        },
      });
      return resolve(chartJSNodeCanvas.renderToDataURL(chartObj));
    } catch (error) {
      return reject(error);
    }
  });
}

const base64_encode = (file) => {
  // read binary data
  if (fs.existsSync(file)) {
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer.from(bitmap, "base64").toString("base64");
  } else {
    var bitmap = fs.readFileSync(`./uploads/profil-default.jpg`);
    // convert binary data to base64 encoded string
    return new Buffer.from(bitmap, "base64").toString("base64");
  }
};

app.get("/", async (req, res) => {
  req = {
    ...req,
    params: {
      playerId: "2232",
      stageId: "130",
      option: "1",
    },
    user: {
      id: 231,
      nom: "DTN",
      prenom: "FRMF",
      email: "dtn@frmf.ma",
      adresse: "casablanca",
      tel_p: "0666666666",
      tel_fix: "",
      club: "MAR U20 (M)",
      logo: "http://localhost:3000/uploads/logo_frmf.png",
      slug: "MAR U20 (M)",
      clubType: 2,
      genre: "football",
      categorie: "U21",
      type: "owner",
      sexe: "m",
      gpexe_clubId: 243,
      saisonYear: "2022-2023",
    },
  };

  var biometrie = { head: [], body: [], imc: [] };
  var gps = {
    columns: [],
    body: [],
    timeMatch: 0,
    timeTraining: 0,
    dure: { values: [], labels: [] },
  };

  if (player.gps.length > 0) {
    gps.columns = [
      ...new Set(
        player.gps
          .map((e) =>
            e.gps.map((l) =>
              JSON.stringify(
                Object({ label: l.name, id: l.gps_id, unit: l.unit })
              )
            )
          )
          .flat()
      ),
    ].map((e) => JSON.parse(e));
    gps.body = player.gps.map((k, i) => {
      {
        gps.dure.labels[i] = k.title;
        let finded = true;
        return [
          k.title,
          ...gps.columns.map((clm) => {
            let p = k.gps.find((o) => o.gps_id == clm.id);
            if (p == undefined) return 0;

            if (finded && !["mm:ss"].includes(p.unit)) {
              gps.dure.values[i] = 0;
            }
            if (finded && ["mm:ss"].includes(p.unit)) {
              finded = false;
              gps.dure.values[i] = (
                ([0, null, ""].includes(k.rpe) ? 1 : Number(k.rpe)) *
                Number(p.value.replace(":", "."))
              ).toFixed(1);
            }
            // if(k.type = 'match'){
            //   gps.timeMatch += Number(gps.dure.values) ;
            // }
            // if(k.type = 'entrainement'){
            //   gps.timeTraining += Number(gps.dure.values) ;
            // }

            return ["mm:ss"].includes(p.unit) ? p.value : Number(p.value);
          }),
        ];
      }
    });
    gps.columns = [
      {
        alignment: "center",
        bold: true,
        color: "#000",
        text: "Étiquettes de lignes",
      },
      ...gps.columns.map((n) =>
        Object({
          alignment: "center",
          bold: true,
          color: "#000",
          text: `${n.label}\n${
            [null, ""].includes(n.unit) ? "" : `(${n.unit})`
          }`,
        })
      ),
    ];
  }

  biometrie.head = player.biometrie.map((e) => {
    return { text: getMonth(e.month) + " " + e.year };
  });
  biometrie.body.push(player.biometrie.map((e) => e.taille + "cm"));
  biometrie.body.push(player.biometrie.map((e) => e.poids + "kg"));
  player.biometrie.forEach((e) => {
    biometrie.imc.push(
      [null, "", 0, "In,f"].includes(imc(e.taille, e.poids))
        ? ""
        : `${imc(e.taille, e.poids)} kg/m2`
    );
  });

  // charts
  var countPresence = {
    présent: 0,
    absent: 0,
    "pas impliqué": 0,
    exclu: 0,
    "en retard": 0,
    blessé: 0,
    exempté: 0,
    "devoir international": 0,
    malade: 0,
  };

  var countTags = player.countTags;
  for (let training of player.trainings) {
    countPresence[training.status]++;
  }

  let graisseuse = player.capacitie_physique.filter(
    (e) => e.type == "graisseuse"
  );

  graisseuse = graisseuse.length > 0 ? graisseuse[0].valeur + "%" : "";
  let repos = player.capacitie_physique.filter((e) => e.type == "repos");
  repos = repos.length > 0 ? repos[0].valeur : "";
  let labo = player.capacitie_physique.filter((e) => e.type == "labo");
  labo = labo.length > 0 ? labo[0].valeur : "";

  // Matchs player.matchs
  let matchs = [
    [
      {
        text: "Match",
        alignment: "left",
      },
      {
        image:
          "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAbCAMAAADF2xaWAAACYVBMVEUAAAD8//3z/P7///p2eHmKcHmShoeg0vT/5rnY+v/33sGVjsfHkZu2e5vHyLxzmdKPn5vj8fT08d6xusLN06xdV4+scFd/cG623PT66c1zYH+ggaDpvYSVn9LSy9hziba2lLHSmZvSnHP/58JojLax2e+En8K2nJVufo+ghnOg1v9dlM3///+2pI/Sp4Ss2fr/9ONusOOgnNLptaDSp5v6yI+sw97p0Laxp9LpvbHY///py5ugmbzYsKD/6dL/78L09/+s1v///OP/78L///+bvd7Y//9XnNL//Pr///////9SUlJ3WFLS9f/x/v7v+v7///H//+liodj/57b0/v/p/v/e8v/F8P+s3//06uzF9Ony8en//uPj4+P/9tKsvdB2pM2eiLZXfbTmx6C2hKD30puYnJhSUpjpyJXhtY2EhISCdnBiamhoamWNZlRfUlSxcVL6///j///Y///p7/+25/+22f/j9/q20/r/+vTp5PTp8e/Y5+/H0++EwO/S/+n6/OnN7+m20Omsy+mKuOnp5+OVsOP/9N7/7N7C096gw96Pst7//9jp8di8xdjS/9L09NLN5NLCtdLp/M3Cy83/7Mf01sfHvcd/l8f/6cKsocLS5Lz0yLbNyLaguLZ/ibbp0LGEeLFScLH/4ayxuqy2jKz01qXp0KVohKVXgaXYvaDSuqCxraBzhqBXaKCPhJvvyJXStZVzapVoYpVufo9ifo+VgYp/aITpsnl5gXmVe3lXanl5Ynl/anPjqm5SUm6PaGJiWl1SV13ChFexe1d5alebaFduYld/Wle2eFKVYFLSIqR/AAAASnRSTlMABTws7uOGeXlGOfz6+vn48+7u7u7u7ujj4+Hd3dzX1NTU1M7Mx8PDw8PAwL+0tLKll5eXl5eGhnx8e3t2dmxbWEpKSj88NzAnEAufP68AAAF6SURBVCjPYqAfYDLXZGHRMGPCJc/Iwdrln9Hmt4CVnRG7Co4989bkeXuvnDL3ADtWBU5idV4xBd7ei4oqM0Q5sakwTQr18spMS+v28go4bIhNhVKCn9fMCi+vOeFAhfLYVPDMLguYHuLlFTipMK6PF5sKxfoC7yAvINiRszlKAZsK46iT3vsjvLyCfbzT+42ggi4GcojQcRY+5e29OyvrqLd3uogjWIhZJ+6YtzvCEMvGjfOB3ok5smKiBVgAMDu+Xce9VZDD1FZimq+XV3utpA0jiGuyZL1v8HZVT2blbfowNVxsaoJC6myuIHl72QmJvbP8AlZl75wxmRvD1R7WUqWpLR2rs9c2gXwXmOKGIs2lK76luWFpcXJ8mFeyT1IYSIUVigrpztbY2BKg1uUnEkKm5q3z9epJRI2nheXRVdHVkf6goAnySs0P8lrMj+oImfyt8TVABUCwzzsn1/tQ7kEtVBXMegI+3ihAG0sUOCzbG7FhUzjDIAQAtEp0elW30NoAAAAASUVORK5CYII=",
        width: 12,
        alignment: "center",
      },
      {
        image:
          "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAMAAABOo35HAAAB1FBMVEUAAAAiHyD/wQAiHyAhHh//wQAcGhsiHyCFaA0iHyAkICAiHyAbGRoiHyAiHyAiHyAiHyA3Lh4iHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAhHh8iHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyD/wQAiHyD7kwAiHyAiHyAiHyAiHyD+wAAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyAiHyA0FhgiHyD6hACYdQRyWhMiHyD/wQD/wQAiHyAiHyD/wQD/wQD/wQD/wQD/wQAhHh8iHyD/wQD9oAAqJR//wQCNbgpgTRf/wQD/wQDxtgDrswD+rQDgqgDcpwD/wQD/wQC2jAAmIRf/wQBGORkgHh8zKhVURBlsVRX/wQD/wQD/wQD/wQD/wQDJmAD/wQChfAH/wQD/wQD/wQD/wQD/wQD2uwAwFhjWowDClAA1LR6DZQ5cSRh5XxFgTRVJOxn/wQD/wQD/wQD/wQD/wQD/wQCshAAaFhbjrADosAD+qgAgFxfQnwD7kQAjHxaGaA3/wQB7YBF3XRL/wQD/wQAiHyAjFhf/twBZ++XvAAAAmHRSTlMA4CDm3DDOwcqw8vzV99AVB9Qi6+KgkGlUGhCX7bQwKgnxvYIe1cRYNfXkp29mST05Dfrw792ti0Qm/M2jfnNOQd/JeWLz2dXPyV4SCbmchFBA/vfThiPn4dnOyLkq+vfz8O7r5tzX1NDPzs3Kwqehlnnj4dRJRDw0HP7z6uDYzs3JxcCxrZ2Qa14y+vTz8fDo3NvOyciLblvWhEgAAAr+SURBVHja7NwFbxVREIbhAYq7ByhSvMVbJFwgQIIUKBRagru7u7skEBxm4M8CRcLuPQ37wcnuNPd7fsKbs+fO5G5WiIiIiIiIiChfTVfOPzra0KJ5aj10+uyzrtLRXDvboEW52LxFOpBrp7VQpTMdJlfTOS1c63npELYcVQ92rRP/uh5UH476r7XFSyvV7d5rNW1XP3aJb2fVE9+3/CZ1pdX1BPFefTkjfjk7WKotu8WtM+rNU/GqqVW9OSheXVF/ropTzepPszj1WP1xO5j62KCTDolTh9SflibxqUEdui4+uYzldSx1Gcvr/xeMxVhtGAvAWEUqqUNeY6lHjMVYbRgLwFgAxgIwFoCxAIwFYCwAYwEYC8BYAMYCMBaAsQCMBWAsAGMBGAvAWADGAjAWgLEAjAVgLABjQSrkZTb1qLP4pB4xFmO1YSwAYwEYC8BYAMYCMBaAsQCMBWAsAGMBGAvAWADGAjAWgLEAjAVgLABjARgLwFgAxgIwFoCxAIwFYCwAYwEYC8BYAMYCMBaAsQCMBWAsAGMBGAvAWADGAjAWgLEAjAVgLABjARgLwFgAxgIwFoCxAIwFYCwAYwEYC8BYAMYCMBaAsQCMBWAsAGMBGAvAWADGAjBWkbaIT+oRv/nHWG0YC8BYAMYCMBaAsQCMBWAsAGMBGCuaG/tvX2rssebUzu9O3e/RePjCixJjpex/9+DhiSN9go4cOXGbsX7Ze3eb/VDbpapq0qTNx48fn/RNVVXV8i5tBp/cw1jf3TpZb393j7FUX560TOouMNaOXpZNl2OVHmvvMcuqy4lSZcc6sM2yx+p2qaJj3ao2JNYaDbomPkU+V9UGxbqvQZvEJ41pzzbDYu2o4FjHDIzVWLmxLpnFOVmXxaeYD2E1HKuHBm0UnzSeUxYr1sOp4lLEabSvhQwZv3joL4uHZIs1sH66eKTRrLFyo9f2lz+NrMkWy6znSPFHY2kZXJ5qrKT1yxrLlo8VdzSWw5bSd62kQLGsZrZ4o7GctKT6ZRLQPXssq3F3zWssXSxphoT0BGJZ3UTxJdpWaEnj5f9j2ar+4opG0mhJs2PEslXLxBON5IEl9JMoscymjRA/NJITyclhSaxYVuNoPtVI3lbZn2ZFi2U2eZA4oZG8SsYaGTGW1U4RHzSSm4lYtZIt1pr2YqUMExc0kpWJWEMix7Ke48QBjeNLMlan2LFs9AApnv7iPJZNk+JpHPsixwoYVvyqWGysD0Asswngo+g11lYgFjA6pPQUSGXE+jjXwhYIwmuseVFjbRyx3sLqkPnUbaybUWOJDK+1HzzNp7FifY4cS8YOsbBRklnFxJL+3d2drZxjjc4eSwaMsrD5/SWTjh6rOxBLZHi9BdUtlEycxlodNdYV+WnWYAuaO1OyqIhYm+SXiYssqH6WZFBhsWTcBguqmyN/VxGxrsofVvR185voMlZX+dOY5RbQe4zkrqXQWI0a1FkSRgaXnyH5DxANBcaqOd5OrHWSssAChkveLhYYyywcqyRlpgQurs2St9MaxZt/i7VDQy5KuTGBVXG65OxJgRO82SkN2SVp4VVxcN7jw6dCY93RkGYJGdCz8Ftrd6Gx7LAGXJagOassZZLkbHuhsapfa5nWJgmbbWl5Lz3Nhcay2h2adkbas7joMf56SSPYmjHWeCsz8EBg2QkbUW1JyyVn5/KMNc3K9b2bWCNOS/smWMpCyde6hhxjTbeQO/v1t1JXad9SS5kiOXuu/+9rdXfy20QMhQH8g0SKYBQSJWn2ndCmQFJAFSSEtqGpOLAeoBJIrAIhbiwSBzhxYEfizCf+WQRi84xnMgOO8/K79/Kp49jPz/bnkGGlktRZ/TOVf4ogy3PfSDTwId4NGRbOUO/RnS8/PEago1TVYN1re2GV1qmX/THleotgA6oOwLqLb/43rK9hw8Jakj5evbz6HlOU3BEjJEmzrfBh4ViWPq5/xFTrVGEenj+wFRZKNfqopTDNtoSwcOHdf81OP7nCCrSSpV5rE1MkRIQFvLjyH+vEh06EsJA6SR+J0mKEBeDWhyc3b9yP+i92+va9ZzFGCQsoZqmXy0QJq4r5u3jhwt6P1/frUStiWEgdod6hJgKMqIhDhhojiB4WUEhS6yACrFKxDBE2OeuwUI9TZz0NXxmqliDBUc4+LJT14/wYvopUbUCAjGMjLODoCXr14OsUVUUIcJBWwtJ/ioXwVQcJp+uaOVth4fgZug3gJ52UN3PAhNbCAs6vUpFNw8+Yqjbm73iWXvEjB3srW8XeJF87azQsDA+EHbJ251778yrQY1cZHcqDg9vmwkKzEnLHZkSVhCOu18KMuanCsoGw3Kuf9QL8pShvfG/QbRNajUrSUFgon6qSjG+kEGCHqsOYvw26TOCnlHd8woquUS8h2EmBBzYTVMUQoLHsE5Z56dW5t7N5rUZbgE0cS2H16VLH3A0ZcU2x5dgJq0LVOuavT5VTxhRH7IQVE3aw9bvzjFozGlgJq0uBQ9bR6GWQqo2wdgXOsjxhHcNUJ22Edc3TbySA+zMcY6pNC2F16HIZAvSpGoT4Ewth9Six8Dekqo+puhbCcn/qznFIkKTiQBrT1C2EVRVYy9LUbnclfIb1uZ+vCFeAd9YEDPATiRMHXb95roNgtZmHlT5M1TkIsYcu8QaCdJyZh7Um7FqaoI2w1QICLHHmYRXpIuaa6tIJehzaacLHloUSTYWqGMQ4SY3WmeJxfZeohbCycm6kCdsV4iQmJbjsJC1UStfoMoAYzRH9OMunto7jt0aNtBBWngK3on8ZOgywOjlWPzYeD1Y2EqSVsGISF9G/VUxu35uf+W1BkmZbUlinqGqlIUpzJCiscwI3DBUNOWFlRJayFAcYwUwXbj2qkjJKWdqN6egOwKglgXtgLlkxYdWoOg9p+pQSlrtp84S8d8S2xYR1kKpLkGaHUsIqt6jagTDlrJiwjlLlCHvnCchTTFgjmds6f5f/xITVkdgP4hpTxYS1QxdJD2L9kKNHrVgCGoW2YzmsJYo8NfdHl265MX5KFTeWlh17YZ2lahPCTOiynYKiXDxzwk5YHbpIeyESl6g6V4bHuGUlrB5VhyBNLsy/ftc/rRGMOSLgmuBA6XB9BUX6ScCUdFV0QVlz48uBCNuLhldvA5ldWX8ZhqwfdenjJEypSJ84IBP2lqrD1DsIU2IST5AH/lzn4KNNLScDQ9bE9oP81qRLI1rleROm5AWeQPnXNow4NZIFGJOTvgem+bycLnRK9LrU68CYYxRffQd2w909NKHb2TWYdIou4up+AAZ0i/Xhkap6skrBqDhV2xAo3aLHvi5U6TZdTjRgVJ3iJw7fXaZGbLSCPzrLEaZXhkqQ83+WVadOvWS7W8wAQD2fpNt6GWZdE3jaXmcf/Y0qtZiNR0Azgp42DNRwGFkGZm0KbiT93y2LSzCsTVVVWAvbH+kEIxrArGaSqjMQKxVnJNswrC+7kVTVOccoujAsT1WyCcGO72N4uzDtkKeiKFuCYbVh2lB+I6mqdDjsgFWGaRtUtUR/hd9lDjGMRBnGxRfnt/CXZsXhVJfTMG6NLn0sgMZSkoGqK5iBClVZsTNSVfn8kRx9LaUwC4cXZF2o01m5nNWO7HXMRIYuYyyUdKFFt0Qas9FfgG2dYPV1a89P7SzAts4URWvXdqxIb+cOYWRrCZKXeT+IzPNZ8cUoKEdrWuxhJoaLN33XOGTn5esKVT0soryFipzm8P8Qi2iLNjaJL8/4zPU39mp8EuapmgcAAAAASUVORK5CYII=",
        width: 12,
        alignment: "center",
      },
      {
        image:
          "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAMAAABOo35HAAABnlBMVEUAAADwGxvwGxsiICEiICEiICEhHh82FxghHyAiICEiICEiICEtICGjHiAiICEiICEiFhciICEiICEiICEiICEiICEiICEiICEiICEiICEhHyAgHh8iICEiICEiICEiICEiICEcFRYiICEqFhgiICEiICEiICEmICEiICEiICEiHyAiICEiICEiICEiICHwGxsiICEiICEiICEiICEiICEiICEiICEiICEiICEhHyAiICEiICEiICEiICEiICEiICEiICEgHh8iICHwGxtHGRsiICHwGxsiICHwGxsiICEiICEiICE6HyEiICEiICHwGxvwGxvwGxvwGxvwGxvwGxvwGxvwGxvwGxvmGxvhHB3aHBwmICEbFxiQHR/wGxvwGxvwGxsiICHwGxvwGxsiICHwGxvQHB25HB0vICEaGBl4HR9TICFyHyHwGxvwGxvwGxvwGxvwGxvwGxsXFhfGHR4qICGvHh80ICKkHiCbHiDwGxt6HyJmHyHwGxsWFRbqGxsZFRapGxzXHB16HyI4FxjwGxvwGxsiICHvGxsWFRbI7MgDAAAAhnRSTlMAwLBP8kDcziBg+evc1PYU3BDunJYJ+3AMB8/LrYeBbDjm1NTCkHcuKiXm4dhbUfDn5Me8tUgzuUQcFt7bpJ99VjAYBMwO1qiWjGg707BjPfnp4MWcfSoJ/Pn06eHSz6uiZWROLxbu4trZzsvLuYdwWzYi+ufi3NjY1tLQy0b9+uzd1c7NHEj+eW0AAAqYSURBVHja7NdLakJREAbh1g3dRQjizGh8zGIwZJiHDgOZNi47YAgZ10T/C/UtoTjdnC5JkiRJknRbs+X28LQa+pYuw3qyOe5qZE6boe9mvn2t8ThN+r4eFmPJtV/0/Q2fNQbneUfYzCrebtUh3vYV7hzTqnsa/rb2ITP4672iJez2f5dlBTt1lq/kQXzsMMeKtew084r13HFit9Z+6DiLCvXdedYVatuBUi/qaQd6qUxRv/c/H5UpcL/nnjydaFOZOtGhMnWiaWXqRMYy1pWxAGMBxgKMBRgLMBZgLMBYgLEAYwHGAowFGAswFmAswFiAsQBjAcYCjAUYCzAWYCzAWICxAGMBxgKMBRgLMBZgLMBYgLEAYwHGAowFGAswFmAswFiAsQBjAcYCjAUYCzAWYCzAWICxAGMBxrqnSWXqRMYy1pWxAGMBxgKMBRgLMBZgLMBYgLEAY/2wU4dICAIBAEULSb0B1zFyAAhUhwGiytC0sHBrg8Hgju6foe1/R/jhA8YCjAUYCzAWYCzAWICxAGMBxgKMBRgLMBZgLMBYgLEAYwHGAowFGAswFmAswFiAsQBjAcYCjAUYCzAWYCzAWICxAGMBxgKMBRgLMBZgLMBYgLEAYwHGAowFGAsw1h6a+zhU3elbVw1tbayPy9iFX4rWWG/rPIW/+iUqs1jrfAsJjs8lJq9Y1ykkKR917rHW/hDSlFt0WxnFaqqQqtjOecd6kWdvP00EURzHf8oCTYtUipReLCKKgRbLJYAQRIFgRCUEIRARJKgRgsZoghoTjSZe5sB/7QMvu92ZdtYcdw/u570v38zpmczufyR731pjHWt/mwIoXItzrKfbFMRonGM9aaNAyoZY65BJcVqkYCqGWLuQSTF6RgG1xDfWVoYr1o8cRFJ83hBXrM9DA5CIcRMmSSNZmC11zzWdql6ZtYp1gZwqBFJsvpKP0znVBa+iXSyiEuRRXI5TVKs/jVqJo6xlLLqVhjSKyyeq4UzAb/4kYxuLZuYhjGKgncLsFDSmd8g6FrXfhCyKyzJ5laA1YhnrVH8ekigm33fII5WDlncbXmwQi64vQBDF5OCEPHrAEovKvZBDMXl9RB6DTLEo+RBiKCaLh+Q2BL0uy1hud7oghGLyq0hundDrsIzlUUxABsWktUJuK5yxKDUFEdhiOeQ2yRDL7QYkCDnWgmUsn8sSXm3+Uaxm6CUsY/n1CXi1YYtVZo2lkWpGQDGORdluBCM31gxrLL37OQQhN1ZLCLGobxABxCPWuQdkkJmAvZjEQlOGDO7CWlxiYXqMDMbTsCQ3VoU3FvLGUSwswJLYWH91ddgxxwKqplGsDMDO/xXrar1YSBRJz2mGjXjEWsepfD+RpPspW6wR7lj1R3EFFsTGcrjHsP4oOmk0FLtYyHeS1jgaO+Ox0uSStIkFdJNWCQ2d8Vgd5OKYYr2Fx4RDGu1pNCI2FjHGOg+vgduk0YOwrUYYiwqGWHu+n42Tn9OBkD0ON9Y9chsyxFqCT4+Eo7UbbixYbcMN+M21R3+09qKMNWqI9R4avanIF+K7kGM55FI2xNqEzqQT9dHaZLw6BI91+PNY+Q1Drxr5t9e1cGNVyCU7Sm0fdMvQ4BLVmEG4NkKP5ZX84jtcL2GQG6EajxCq36s8sVJ2sfrIZ3lLeazB6HnUf/FLYT4ro0B+2wfK7QWM8inyGkO4Xg2zxPrT3r39JhFEYQD/ItsISwIpt9baCiEtiIJiaUEQqVVr09Z6i1GjJsZL4ou3+GQ0vvr5Zxs1Nc6ys+zEYfeY8HvmgXxhZ4ZzZmaH4cJapY/Sl+8hz8+tUOUgYh+shFUNF1aBvm4+/jMVfkSALj1OYCJ5D+LxpXBhVegv9+nbL3e+IlCaqiQidu2YhbBOhgsLa9S4/vhnVi8RbJmqFqL28Fl0YSFPDefNo88PMMEpqgqI3LV33/91t7L6GCagt+BSYbQFq0XVKmJw+4bVrd0JBNhuUiOziQkuUnUBcbj24t4/hfU0fFgoFqhTRrC6N17E49rLZ+ftPoZ6iTQ1Cgj0ih6IzbXb7++/vXH3vOkI9v31ExqFhasdaowQZFtOWH+ruzShC8t8nH+OAAcyw+pxumHhVZO+0ovQu0JVGxLcoqWwzMf5FPS2qDoJCZq2wjIf5y8brOCXIcAtRhEWrvY4rge9S1SdQvy2aC0s83H+EFp9egi4LCPByMJCvWpQ/mzRQ8AZ6nX6ySyv5XcPh4ZhmY/zXWiNqBoidnWOy40SRfzSqK1bDQvYTIf9b3wm/qJDiHLm5Sz+Mkgt5yyGhcEG/3Dr0KoLHLIu0WsfY+rl5bStsID9o/DdFvRqVLnxH9k8CL3dde5yyVJYmF9Yd8ncqIEAHao6iF2ZHnloNQ51YZk7kc1O+ECGqthPII6XyXNZBEhlNGHZV6FHA7Fbp2oPgQ4yUYVVlrdwQIaqOoKdiyqsPFVriF2SqjQmWYsmrIZDlYC7MeaMp5yddCRhbVHlCLhd64quYqK3EElYewLLMynzkSHrRhDWTo6qFOJnHhawEUFYKYEVB7SoGmGyUQRh7UqsKF+k+dCwFkFYZwWdw/+jQVUmiYl60w/rKj1kXGPqGv+0TjjTD2tfxC6HMaumGxCwyemH1ZNX9/M9VOQmEGx5+mEtulRtQoQuvdwWgjRK0w/risDlu67Bmp83mNTJFmy7TNUhhChzXHph0aDqkIJtEpfvvxTT9OHkE7551Z0Iwpqjh5zb4svUWCq0+lBtpxlBWGtUrUOOKvWqe3MncGS+7DKKsIYiLy79rcJAmc6pc93BfL22WiKjCOs0PQRcxKlda+lFE1aNqksQpSMqrHV51fe/zTcFhTWgR/wXvKr6F+SEdYuqHKQ5XRIT1iFVlyHOWSlhFTNUXYE0KTGPYYIqdwfCnHbEhJWnqgdpVuXMhm2qyhAmwX/RgkUVevQhS/ES/0UCFq1QtQRhtignrKrAgwJ/23HkhNWgRxeyLFBOWAsye2CBpwY6tYunk/XE1kaGk83BnlWZPTB995e9Po4UN0dOhGFlKbMHpu07cUltPBWvbJQYpAJrWlJ7YNrSXwVeg/0q9bZh5n87YRjUpDsJHzvr1KrDlhOO1B6Yrkxahp+dS9RJwpZNuT0w/wmoNNA8rRGEVZBwn4rJmnnXtAeUgTUX5J1ACfyGThIaLv11YMu2wBMoHk7IW/2b9Lc/tXm5CXHaVBQMa15ni7Bliao8xBmGPPdxkn7aXdjSp/SFA9CjKt2ArxzHuKunsrBmn6qMuOo7sEePYTLcbFhaGcCmDQGv+TCvKbcPwmwldSqwatEVv3A4atSpVhYn98ouwq6E6M0zR3Y5rl1W46qR056q8lSdhUQH9OPka635In56VRtyTBKWteUvHCZ0Dd0LZ8+UInnJUkXgnRd+ujT3HJat0GMeMhVobADLlgTvulUUmzS0AcuSkl5ea3ljyBwsK0tvGCo1SiOHsK0ne9et6jkNtAdQ2V++70GyLYaWqcC2c/SoQLQyvSJ8/+5Ietve6+IZhtE8DfvS8b8q01A2z8lOLsK+g//iT7THq90Sg6XrmIIVebewhdGvLVGvWstiGqrit3Pr9FOFKv10tjEdDXoMMDMzMzMzMzMzI90Pj/pkyjfP89IAAAAASUVORK5CYII=",
        width: 12,
        alignment: "center",
      },
      {
        image:
          "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFYAAAA/CAMAAABNc89XAAAATlBMVEUAAACZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJiZmJgr9Cf4AAAAGXRSTlMAEvUc6jDfRV69fnE5p8+KVWiwk07GJZzV3YycQAAABN9JREFUWMPNV9m2oyAQtNl3EXDh/390EJOJgpmbe2cepvOSI1gURXU3Dj8LMJwaQw0d/lUA9moNju0xx4lS/C8whZWMoPwIxNy8ir/jjAc+BvYbj20M1b9bmP6CMQgVDsjZRu2TSGIMciPlCVn1T4GxtrkGW+nrIebR7aTJmPCPUBU5UEkjJTXc7mLIHygBdEQPVE27QS/3kW36NlUcn4fPxM2iuo667woMcVcAOWsZknfLjseSCr4Fq1BGYRR8MWvOdxP4Ifzmv8EXPMrEmuP9Md7N4A87S/E5LncIWfN7vjHQapDGTFglbPnHZFXZnXhh5KwbVB4zGo12KJexT+UVZfb4mgxbXjlcUENGOg3YqN0OE3zo2JxjOs31CKlLTs+ZeQrVECyj8FnhmYpmZwZAZQ4U4yfXHXVangmOMtEfwY4tAcwZkSGsShiMQbk8iwWeYwHlgD+xgcwoNbrMjzyWdo0sOwHwOghSzPuBtCPKxDSysHwKxvGJHnUZrfA1rM2o8SLd8jmmrtDJr2GxzcRfF0rkAjteQYzLjn6QYZmZK1mXL2HxdXjO5OsK6Vne8PVJvsbs4QIrMwr4g9rFzHLepGxgG59CeWP7UoWYmyObchtTtz/2JeyaT+UDFj6tX8GmLTP+HhDKb4BQG7gAoMKLUZLcxwgdrP+DtXjyAxxKBhXnByLqYO1yddj8BxEMNSuxgtoDCx3JGrUQtoUlyuATGz+/FwEn61BGCtTBjoRRq4liGCB1uEiK4cmY4hW9E4HSVe5wsx4EqagjhVffimchttUWnHUCXOnQSPI9WzhA0Ww13bOshDTX8d+4zE5AxzLdxWnZh1SlQ/vz5zruoMxybOoBlIi4WZccoFIDDAAmsrK0MsB1FcXirnf7dQclwVP8yMW+7y2VFPldaIH6UBYKk32cYVtYfL2loSCe9wh85EN3x4+Z8LMBaHoltTONqyRBtXmeelcVt7sE0YBGaFaa5mfxuUrGZQUd03DuiHFPpFYsw9B010hqSHxhsAtg19QwU3dsBUG9N8XjIKfrYgTJdelKZWKF7dIRI6kv+duR6XBZS7p4gPZ7c6Krl6S3/OFxchGdW5vgTd/pygnEu3Liqwj2/BGxCPHu86rWhYibO8ncsxX1yNQlcfj7/mOKDEw3/TjcNajWNEvLEDCcLClzkfds/hmpHnZsL4wwtMkZVnPenisvaDi5484Ic85kWt6hAh0DubYoEMU7m16e5xAQmaD5cE1yL9DvpASqLes/lPReRldOudbKlv9IarhkqHaFa3yDCiapAorI7JsBzVAhfOqRTrwqe0Gtjn0DynlNQCS9gc6VM2p62JgOVajXMu8i3foeU6pqBZPC336gr20Pc6K8g0HMaHf2G4uaWD/ineqNTjnnGLiXDTCTcXWO1bbH4ZbqcbeYYz+8YDU7Oe1CjLZVooq9RXEHCkuUdftW9ztZDmvoejYmBodaylYD3IHq2sCc5QNthnYgVy8K6UmdKznPG2OEMLY5FyaK71QtniK1K3LAbRv2Wsu6VUWXk2IYcyGUmjw3dHljf8VqE06ma8OJT3UMab8M3wuo9reT6DeSDr+hVcDw7aAByYkP0B6U0YzUbSgPww/CqHTjNyVrZbDRAww/CugfpOJiglxQZsHDPwsodP2kRXHOfxW/AHdfcgACXBhDAAAAAElFTkSuQmCC",
        width: 12,
        alignment: "center",
      },
      {
        image:
          "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAEsCAMAAABOo35HAAAAh1BMVEUAAAB7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3vsNqSjAAAALHRSTlMANDrGj8GSBgr2auPZ73CK0HUT38tX1HmuHYRDf+e3DyJKZBjdUCxdpCedl3Ww9yAAAAuSSURBVHja7N3ZeqJAEEDhAoMIigsKogiKC271/s83iTPZWKRwULvt+q8z38QTsKFpABhTQbqeB5Nx5ERjP27bugmsRDqdWPjL9hwCKxAGWGSy5s0rS4uxjL8H9oM5wGviI7BPqxZeZ3nA/kocrDQH9sFDijN8W3mLc2xMJkZ8mK9TlQYAD2lO8JeWPcDYnhNQhI5UA3i3i7FAb+SCAmYOknmwa2GJ7kiBvdFAOifAK4YvvzP2sUFv8NJmFjYp7sALO2CzhjN4WUdsmr+BV3XGxg1fZFA0V/reHk3tdXJ04YNrYZHWYO3tp7GFN4lBfiv75Fv4qRss9JJj90D//BcDvMkI5GbuAwuzhnZcMb2g+XiLFGRm95AqhJ+WPbyBAfJKhkhmZ4dLC2+wA0mZ7f/aJqZ4Ax/ktGphDQlkdRx1Ni3NwRq6Zo1j/MlpvmgbzuscPugW1hFAno2FzhpczEYRFliBdFIHazlDXoIFfB2+bILCgUI2btTAFEuIeUMXfmpjzgFkY+B9Ylmzyv+oK9sZ4gLvFMsmzIpJdlX2iI3ESjBrbBJmLySbYo7vFWsAORpm9UEmOt4rVgI5nQgzFiAT426xjpDXym9+EkmxmOMPu/8Za0X507RBIgMsELX1jWkuj/0Jx/rB7GLeeQOfbItjfdGrTkHSLcf6NMKqwfzocKx/DtWzJl6jsWS+kt8iDPgTjnWxjAjTcXuOdTGjTDBtrOZitSSOlWJWCHk+5RPuXj6WhlkaYd+hxbJeLZbOsTjWxevFGgBAJ036g/b5bW6HR4GnmQWItewHY/zmn9aiLv5+fqwowiznJOa9i8+PVSzWQTyixkI8ibcz3jXWrDAWVVe4RSMCxxJvel7oWHgCoTw81gTrCEAkDcbyqLGk3bZEjyXUNVjhY6FAx6fix3LEOd4SP5ZAX1sSxEJhznzuGcspjCXxfRgyxEINxCBOrLERtyJ8J+4iXUFiOW1tCQBuEog8IIoRK/j+ybCHeYI8DEiIWG34wTWEPXq4a6wNLdYBqm5d7IlxFUOAWFEn9zsJulj+4bGGhLXdgaCL5RuMtafFIgx1iaC3Qz0/Vgw5riPmRM3zY7WLfkjM2fjnx5rLsyBCklhirNLlWK8Xi3dDjsWxbosVcSyOJVisCcfiWN/WuVgu5f6DBcfiWBxLilgvN+tAimVyLI7FsTgWxyohUawuKdaUY5XG6nGsDzbHUjuW6R61UE9XS4FjCXHdsBNOg6GDH6xu62wfYcWxioVvXfzNitsWxyqwM5Cg0VhLSWPpBiLHIr/ukmPRzAwUJVYnF2tEm6h/FN1BjkWUOPicWGP5YoWIHIvoaHEsKrOHHIvqgILF2ooba4cci8rsPjBWHzO2csUa4VNjdSix+oLE6kQci8xGjkU2xCLxWk+1cO4LHMt/fCwNCxxS+Mfr3TlWryjW+KZYC7i3Keat4ZsbcKwvceXNxjHH+mfpVD7qrBM9OtZS0Fg64b1cHscqCzGFnB7HuhhR3kQ8aC7W6LZYthCxFoT3nIH36FhdMWMNMGPS3FqH9qvFamNGq7lYA8p3lm8qGetA+YQ7zDBAyVg25QkerpXd/NSMlX3BbUyawk5vjlV92VrgWGBnMxRZZQcBRWPBIff5qm6iG5rKxoJTRavsBhi7UMRVIhbsWngRaFBOD/Bi2wdQORaAZg/m6yNcl/bPp0HYAdVj1cGx7hJrzbHoay92HKtUgBkrjlXKI5xAmhyr+LawkGORr2aegWNRd8QD0GKNIEuRWKBN8C9rChyrknfyo3E8XQHHojCXHQDgWFQcqxHmlmNxrA8cqwaOVQPHqoFjPVFnzLE4VrPoC085FsfiWL9wrBo4Vg0cqwZ1Y6XwdPLEEuC9zZ2uLLGc1txzoRZ1Y32Igl2dXmrHetdta0ClfKx3QQI0HKt4SWQxZUfD384uVONY/3Q9qMSxvgQbqKBKLA2rbTW4TpVYroUEa7hOkVgQI8UCrlIl1g5J3uAaVWKBgSQnKKdOLLeLJG0op0wsWBlIMoVyalw3vOj7SLGHMirFAlNfTxeDg9HDq1IooVSsT6a2PkRYqteBCq+9MCSvsw+wzAGKKRvrnV6aK4EiSscCCIdYKDKhiNqxwBzUOdpSPBZAEmGRIxRQPhYcx1gggAIcC1yf+rRJjgWw2RIPHzjWu6ODOdYMyr32vTsVEsybQw7HunhDwgM8OVb54/dDyJI/1hstVv0d8Qw50n/BE54mSWHU2A/lXeswx4wh3EIn7oePfLpP8xaY4cNNjIdPMLvRwy9cTjGjZ8It1pgVw33NnIfPaPcJbwyn2FiP/tJKMcuDO1tjhrWCm8R3nYwXY1XwrqnP2MesHdxV8viz97Cpv4+GDx7JPcyw/rR3J1hqAkEYgAuJImArKgiI4hb3//7ny/LiS8Ji2pleisl8FximpIvu6u1KmjmqWv5MGF75MEWFOJBmua+qHxwZLtMMLAxHQ1UF9NJwuTRDhUvauar+xwsq+qRVbKHgeFHUhae72WBthIVrpCeqipwTs83CQVVA2gWoKjoRrLONifACVUEngrXUOWKQ7zvcOxGsDBXhkPRboCLuQrDqpb8+GZDVM3wHguVI17E1N/5jB4IV2NmzsEPVlw4EK9Ob3+XLdhH/YA1DI/m9zkXVlX2weqjKyIg7qgL2wVrpXi8g3xcu2Qcr0l1Tlu+W+jnzYJ1QtR6SGX1UBcyDlepPWfIJoLSyakJejKqADClQs1XQue2TLg7snSQ0DFVsSk3MbT4cQL5raOCPf6WXzXxTE8TeWtHCCotrYO61a4M1OcPmeV7eXEWKPwhDE9KxyVZYN1DyWxW+ka34Pcv7/h01K9l7IX7xx6RNiZqcTHLVPMBmNQeA9e1K2pxQU5JRgaoVtJ6TJMWGNMpQk5BRG4GGhVocnWB6XFh3Q82NOMpQMyHDHNRxfLV4PGdpPW++9Ut0IeMKgOPRrRUJwOLo4gg1I2LGC1HjkgUJ6lLi5Qub1z9GjX8lThzU9cmKI59HaRGxebGanyUgPlacfs0dWDdEBw16ZEuJusgjHoZzsOhjPWzRYEA8XNDgQPYMAOtj+hYBGizJos2aa9py0CAckk1n42etye/xrTuSXSXAcUQdo0FGlm2tH9Yqndz9A9k2ZnBsctWEw8HXL5wMPCZ7UrBshI81pg3OZEuAJmJGHCQAp+7WHuD4JXy4gdG7FVg/8fo5bwGASQViCgBMyqPNtj4aTcm0FRoJ+72G344crhL4boBmO+JkiWbZkMyZZdYPnZeToZmbkynbiMHlD3JcNBM7MuMoAIBTJbndbATYTPMrtFhsiJ/rGi2yLem2LdEizIkjx0cLkZBeZ4EWgkMlsknP0iV9sxva+AyuGW6xQ6t5QrokX9HG7xFfOx+tshPpcMrQSvBb1lNZrd1usiHVNhOgm+/VD45AuzDwSCUvCNFuzTdfPThzPBHtPXWh2kd4YsT1O/inPMIzUTAjFYbBAs+4nAoN7bwMT4XLnN4rT+d44Ftxl3LHc+JS0Dt4xc3ndM3kOwU+/iFKT/Q2pzTCP/gcZr3k9UL8Uzx26FXO2PXxL3PuXYaqTQYJ8aA4kKxDcY/xJw4FR0VSSBHl5JgP6TkvTyalQAWXwr8KvQiS/K/lcl+cNlQ3OxX7ZbbwISnuWhN88L7gFX646Gf35XQc7M/7YDxd3rP+Yu2jHYM9TAoVEUxa8B44/4u3hDnLDmb2vzklzOg79AEkI+j39Uwfg5cK6CXSzrfA3w7LNfQRE54zOO8Il8BPn6GScUjnUC9cdaNu9bLhPoZa0ZjjdLMqu5uAKv6Fy9JHbQ5TFyq46QdMVQ2cNML7LCYfogcqqZe6Pt7Gj1fdHgK+RX6+jfCq0S3geC6JCcNrcnMF5Ij+bX/6QB31N8l75y/9kUA7MSon597/kc+l5M4xWA0upbsYzcO1WIfz0cLNLoNVcHQ+w/Tp06eHb5PRAVYD1tQHAAAAAElFTkSuQmCC",
        width: 12,
        alignment: "center",
      },
      {
        image:
          "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAhFBMVEUAAAB7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3t7e3tM1yX3AAAAK3RSTlMA9gjwy8R5QA3bvWblGzpzruE0YGyMFlsvoCTpp9Uq0JmGtFAgEkuSf0ZXfNRa3wAADmtJREFUeNrs2dmSqjAQBuAOhFVkF2QRZVEZ+/3f78wwHlA0hsxUjVLld2kZU79p0jHC29vb2xvoqRarq8Q16cawvngbqlTJsV1oMBfNQU0UgyCLsoTXV8RHKiOHKcFLi047SnCKvrrSxSKF15IGboZTBfAtJojyCV6HvqhkFFBAx8eODy/CXnkopIRvO+zs4BXogYIM8sbMd6UaBqe6rk9x6LTHpKIWDeHMxU4Fz7d0jLsRaKLG9lKCW9J+eNXBjgrPtiwzvOFVqtbAJFK3mIoEzxWpNzGIstJ0kbrcGcZOh6eSHGtcT26cgihJguc6bPGaGTYwP2mFV7zyZXqBEFXGSzTcwxxpFC9VHzBPKl4y5xqjUa5izOdH0kjs4cAIYK5KHJBWh5mKXBxU89xwv9gW9qwQZuuDYM+cYxc/W+BAZDmkfVr4tm3762IpwfOp2DO0aQkKLSxzc+vJBL8Qkm2oe3TqdQTPs8JergNXVLfuhiCDZR6DAp7iiD0VeGynypCH0NVhCX8tH+Y/cepJO25xKssNdPhLw3pkNjySOhTFeCsbpogW6pXQ/3EO/qXtR07wB5QJy6JZOFb+Yr+ie2CLTWQhRM5kmZ3ScCLOemR4KwYxIf5X6cAU0HsBttXKiTW/SJtPxdquF22iWHjLaiNuDxtTBPv5lCt0zcQxw20P6d0Rkb24c9ftBbyiGNuCCJ/wc6wTvEZM1dZ5/z4kHl6jB2BxcET4IdH72ajOnIXgFdMpYAr9lIyqLFlyy3uwiUCAyc/R/uI6JRrdG2cBP8i2op+Ucv+jg4ncAENDcEAdHQRpLl7KI16QrsQlEFLjGfEnbAaoxMC03APLekdwYB14pbUDUUsZzw7AtO5rNgB2WpNYpcT+jBwvtLxn5ASC+vpV+ecwWZWAScMvLrDZCg5y/XEQ0oAQB89ceGSfIxI3nbBl+A9nk7GnROwg4q3Q72tG4r2zLuARycPOAh5pcuxZ2sMgqIKADZ758Eu6wQ3SCclQPdq9INTEs0LsqMieXmoVMxANwh2Qbock9Z0glZThNxOmasijIXr31ayEg/DsE+xpt0E2Q0M4ibb07B+x5rqcKBBEYRjwgiiCkYso3mNS8v7vt7UbdEqYj5lJimz/ShkFuvuc7tM9ZD2N8manc0KrmcG9depOIJz40aEXFpqXb95A4MMxMtHNCFsuPVkpHMk8bIt8a8biyE5/NoApzWK4feqiJ6X3MnD3urHSpoW48O3QrnZc5zY1M/Jk4X8hySGTQTR7y+D9caklqklZWEwsO41i8wOhMqk7gJgcz+PpV3Zc0k3MuEQwANLPwhnK5DSX9khyXx8/D8b7X7OJ21NmxdY0JSeQNMMZDxBeyQz2dQl5hCNy/p+tpUDkmnrRlSxOyD6/Ob9jx54jjNwIMtOEE5K6de0Xzq+YXzemuF9ggpk3rtNrkvU/t3J5urU+8SQVaGG3MYnEBIXkAOh686Tq6BA+RJa4Cwet4D1eXINmsjNGwVJ9v+0UefzpoH1gGCIJ2gH8UARv+tjffWCrCwRSvek2cwFKkUf4nx+zBs6r3VnybVoEwIuesOZ5iwHy0USepqIdwYPpfkb4VFJIDuTHvBOiPaPZb8IKi7+F+5SdkOZkMD+SKwvEGKHz1o+sEBmSD8SPel70yaUSQk6MjRt2ZR09KrvTQPlYgIAlzsYSWyzgYzygzgfyY7sirFMvCaVyZDWW0/WCgfPBLFkT6o5KyUmdf2lRssQ3+LFnEYZKtymwleoRKvpR1QIkWxlX/tI+H2wxbW5DXumWLjArgmUSLQ42dvzo1TwXklUFoySnvn8k5ynfsYUfwcJomx4I+McOBeN8CoPMQc+POYgKLT/YxjRFbVA4+hDOFRRzZhnnRM8P0ikpBaOgM8MxxWRluqNknjA/2A7Qi1fULG5EkRlcCSs454T5wXaCo0MRAFCWoNMzD3JL5VLDk1DmY29zNHAHuREDeXwogKaz+k6Tk5z0rm7YSwHyybTNdQD3GDKlz8lGo3ftLjijCBfqhdYYegNTBFJLc0RjQeaAEebbP9jLtY6qr+fAKWjrZjyZCuaHniRQhbJ5IyiFcuSIQLis7V5LlZ5Ep9khqEbHuzDkB/WFEApz+gr6HKgwgXiYeiL/PKSG/KAHJkG0UnH6IJSLUuY69xO2BPhhyfYmMKOL6tMZ9CMSWnxzMj0/6BGgnlahqtbsACVnx9I2nI/F91YUyTt8Pu6GkLdkR8fWduSHNh8gnty9upF4aeuBlcpFHKC92OcE4qq3FRTUSePga5B99QO/J1B9LVJySMPx7Fmw7B0pXXXhjJSqrlI/cOm1+qS4/rUuzHl9/yn+hW9r/sKu+LJ257tDa3/lL7wxUrR0QO67nue57rkwqzVy2o5cU60z9pOqqgL/EVQBQb56qkr78BrwOZH8gmU/VIkABws9txrwC1+9Z1j0OXIBgEQyNoY7laqNpAI0gorCr8IWHMm2Kke2Jo5ca2m+3RuAMoAXE0i+/HgEhagJ/sg+I9N5jRMgqOZlp/jcdO1P2uX7jtzBkYmcDaCqw0nETLv2gMeTqx6B0FI58qe981pyE4bCsGjGdNOrDcYNrPd/v0xmEisuvxAE2M1MvsvMxqByqo4OPtBaL1nvWMEXPvjnfLLBQlMurvHb/gTPRtkT0lr7z07/4TXeKnpd13v9zazBQ65SZxWdYgnV7d2yQss6PdYImIbiox0Bo3bKiZadyZMUu87BqukvejIImPke7Jbdpy3dgeRhR0bwJk+lBK4UCZEhX+vjGrcgDNOAVyxaJ8sQXxDwwhnwim+fZDMHEVI7rabvjc30o63S+fzvdvcpF9+CHH09vRJu9DUWFAnKn/eu4X26LKWi/NyZTKA3KANKmqAi10DMrqafDKp9AbOqkyk4FWWoJzIJbWDX6x/TYBlQGRaZRtap0k+JNBrsmoiZeg8c6DTFZxMOfqYhkyn0U58hey4e6Z6AW7pJPiYgPbCwKvkqLBDHgCQvvINwAznk1WjATPbATu6AbgjByq6FvAey7oE7LkfgnB5A0mAdmPBGoiuVojNP9WuFxAM3xODJ1EECevbGFPPy4CSGDMp3PeG9eAVu0jo4EnitFEaqFfCqLuVX7q0UHerGUJtGaDPuVttbeGeVF5gTeMNF6iFdU28BnZmjCvcYx6YtqpWxZbI6N3TKn3LMW4vKg26sOHVltja679VwHI4IRaMBXUfccbYuRneyd7z9eONcxliZPVJAV26ZpYbWMWQ3A1clhOnZnHsjvEOSJftrLAlekB69UM33zyrOFbI1CaFsXgcsQo1UQaKwxVoN2Yb3KqsBG93BS3MbliBfGHybFdc1wz9QcSa3IyuA7hfjCUdulY4r3wqyEiZUlbI/WCBzwmV8+5XlPQJ9QMBbIj1bgFTyanHJmZNl1QQS4kd8lbyiy7nz+GWVBE5pLRKQSQ7uPGBsyeI0nKZ5ppAPm0OHi4Ss6HJp7pxHueDKKLDukgM21xo62JUoCwxf0QTjvBYvyaVkC74kic/pldAD4cF9nAJeeUVPluNS87pX1EB54g1k8nriKAFZDI1XJRLiBYE3S6+8okslI8Jcu1SeUNdpO2B3i4ppxekRJtePx4iuybb9OblnIoa84xaJ3ChILABbgs+kLw9BlPoxJqGWxeRc47ZdDcZpm47ZcI5qpFJKRnTwEVpAR+UfZNfQqPM9LhUqeeHCDlzdxG/sewQzPC7kPnEt35WNpJHHV5Jg7uyHY25XyHx8za7Lb+5D62C+gTT0wQ2eXWHnhC/vhjywJsppcGuJDeRgDvXAbXCHEYzFr/gLSvqgukwaCO5uSDu+I9mSMez4Ap3t6QPD4l/2EtBaWTvY7vqgYHd2+Pk/6ZGZY5guwRwHv+Z4iSnDcEHVKnAkxTWXfYEvyLid+Z6CVhBIZFAGcgFyYY2FX7QGSiIsKUOJ8asGIRYQ2aqfJkTGOa7JTZhUOpC7Pmj0aSjZeI/dUukflOmQ7pECMh7HBpkM2EB8dx81X0Hsi7X3vo9sW4zjqCO/5zBj34lOWfHa/L+0cOQLDOWEPv7dkKgy1Lgfsrxy4L19ZfTmDHcJbGf4gsqGozwl+oJdbXoH/XlgNXv6SutiG8N6aW7JZCo8EsYhp++U9e5oXYMikeWfayBvz5kebnLNp+9oJwLRFXBdbiQmR04YQUUBkr9XVU1TawN+8EINuZkhoLDGIms815qRHX06Bam6cs2yBPpLj8dhG7qSuU5NZNKxGHE26LvOdlaWGGwvnwmXoNtTcez8uh10LuZMCR7YSIyM8JHdWFOoAPs8vRA+2xyMYzIXlW1pS8DUnWKzpBipzqNAHrb7Buha/hckGnDrIIkexWbtSy8qea81m3shC3ahhImn6cgVxWE65nJw9XvkeZuN54W9Gziy8ANj+sAI5q03eiB5ZGky9Q9pKsisbChjdyCABZ61JTOTKvSBYpHlcE3KuJH5KTTK0FyyDAmTDpCVnUfkGY1DFiD0QSJiXu4SZZQbmczMSX2aqpl/H0eE+2jWR+kt+BrUIhyfvb5oNqVyNZ/jf4csAQ7T/a6YQ/zuz8Ow51aLOKHFUHL9b93So0GfaBKyCkVDn1GjYvpinCoJfMBuBdz27Zt70ZRNvb3mBn3Gt8iqpCp9QWk9d9zCpo1BXyi7hKzNXaNv1HnkCumx8z3WJPqK0jnkKwg1+gGj2qRZwvHtda9RFfqOz4axOn0ugYC8bm8b66QHh7OTXJLkXGT6NfTineaD/6FaF/KVHI57ykNSStu2S0WSKAel6smXI18rhf4VWvRdPpTuRO3ksajx9/r6/vneGFQYprG/1yh+J7W8VnwwpRnfHfJt2QZpbPrSwBjUxtO/8SDYaLKTd9upe/tZcCTbqNtmk+oJ+XfYnoN7tInzqjW1n5jtLr91XqoX/9Io/vOf//xnOX4A5KgjYKEdTTAAAAAASUVORK5CYII=",
        width: 12,
        alignment: "center",
      },
    ],
  ];

  // Matchs player.matchs
  var matchsWithRapport = [
    [
      { text: "Match", style: "thead", alignment: "left" },
      { text: "Rapport", style: "thead" },
      { text: "Note", style: "thead" },
    ],
  ];

  var playerPlayMatch = 0;
  player.matchs.forEach((match) => {
    if (
      match.minutes != null &&
      match.minutes != NaN &&
      match.minutes != "null"
    ) {
      gps.timeMatch += Number(match.minutes);
      playerPlayMatch++;
      matchs.push([
        {
          text: [
            {
              text: match.match_date,
              alignment: "left",
            },
            {
              text: " | ",
              alignment: "left",
            },
            {
              text:
                match.adversaire == 0
                  ? match.otherOpponentName
                  : match.adversaire_name,
              bold: true,
              alignment: "left",
            },
          ],
          alignment: "left",
        },
        match.but,
        match.cartonJaune,
        match.cartonRouge,
        match.assists,
        match.arrets,
        match.minutes + " min",
      ]);
    }

    matchsWithRapport.push([
      {
        text: [
          {
            text: match.match_date,
            alignment: "left",
          },
          {
            text: " | ",
            alignment: "left",
          },
          {
            text:
              match.adversaire == 0
                ? match.otherOpponentName
                : match.adversaire_name,
            bold: true,
            alignment: "left",
          },
        ],
        alignment: "left",
      },
      {
        ul: match.rapports,
        alignment: "left",
      },
      match.note == null || match.note == "null" ? "" : match.note + "/5",
    ]);
  });

  let _entrainements = [
    [
      { text: "Séances", style: "thead" },
      { text: "Durée", style: "thead" },
      { text: "Rpe", style: "thead" },
      { text: "Themes", style: "thead" },
      { text: "Description de la séance", style: "thead" },
      { text: "Assiduité", style: "thead" },
      { text: "Fait marquant", style: "thead" },
    ],
  ];

  // return res.status(501).send(player.entrainements);
  player.entrainements.forEach((entrainement) => {
    if (entrainement.tags != undefined) {
      entrainement.tags.forEach((tag) => {
        if (tag != "") {
          countTags[tag]++;
        }
      });
    }
    gps.timeTraining += Number(entrainement.duree);
    _entrainements.push([
      {
        table: {
          body: [
            [
              {
                stack: [
                  {
                    canvas: [
                      {
                        type: "rect",
                        x: 0,
                        y: 0,
                        w: 8,
                        h: 8,
                        r: 50,
                        color: entrainement.type == 0 ? "black" : "#B02C3A",
                      },
                    ],
                  },
                ],
              },
              {
                text: entrainement.date,
                fontSize: 7,
                bold: true,
              },
            ],
          ],
        },
        layout: "noBorders",
      },
      { text: `${entrainement.duree}min`, bold: true },
      getRpeEstime(entrainement.rpe_estime),
      [
        entrainement.themes.length > 0
          ? {
              ul: entrainement.themes.map(String),
              alignment: "left",
            }
          : [],
        // {
        //   columns: (entrainement.tags != null  && entrainement.tags.split(",").length > 0) ? splittingArray(entrainement.tags.split(",") ,2).map(e =>{ return {
        //     ul: e
        //   }}) : [],alignment: 'left'
        // }
        ,
      ],
      entrainement.commentaire == null ? "" : " " + entrainement.commentaire,
      entrainement.status,
      entrainement.comment == null ? "" : " " + entrainement.comment,
    ]);
  });

  let tags = await createChart("tags", countTags, player.labelsTags);
  let gpsTotal = await createChart("gps-total", [
    gps.timeMatch,
    gps.timeTraining,
  ]);
  let gpsTraining = await createChart("gps-training", {
    labels: gps.dure.labels,
    values: gps.dure.values,
  });
  let entrainements = await createChart("entrainements", countPresence);
  if (req.params.stageId == undefined) {
    let techniques = await createChart("techniques", player.techniques);
    let tactiques = await createChart("tactiques", player.tactiques);
    let athletiques = await createChart("athletiques", player.athletiques);
    let mentales = await createChart("mentales", player.mentales);
  }

  // Start
  var contents = [
    // ! Header
    {
      table: {
        widths: ["*", "*"], // Divide the table into two equal columns
        body: [
          [
            {
              image:
                "data:image/png;base64," +
                base64_encode("./uploads/MyteamLogo.png"),
              width: 110,
            },
            {
              stack: [
                { text: "Bilan de Stage", fontSize: 20 },
                {
                  // stage_date_fin, stage_date_debut,
                  text: player.stage.date_debut + " - " + player.stage.date_fin,
                  fontSize: 10,
                },
              ],
              alignment: "right",
            },
          ],
        ],
      },
      layout: {
        hLineWidth: function (i, node) {
          return 0;
        },
        vLineWidth: function (i, node) {
          return 0;
        },
        defaultBorder: false,
      },
      margin: 10,
      defaultBorder: false,
    },

    // ! Player Info Section
    {
      table: {
        headerRows: 0,
        widths: [200, 200, "*"],

        body: [
          [
            { text: "", fillColor: "#F5F5F5" },
            {
              text: [
                { text: player.nom.toUpperCase(), bold: true },
                { text: " " },
                { text: player.prenom.toUpperCase() },
              ],
              fontSize: 14,
            },
            { text: "", fillColor: "#F5F5F5" },
          ],
          [
            {
              stack: [
                {
                  image:
                    "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQoAAAFpCAMAAABTdZYPAAABcVBMVEUAAADMnG7MnG7MnG7MnG7MnG7MnG7MnG7MnG7MnG7MnG7MnG7MnG7MnG7MnG7MnG4yqH/tHCTMnG7///8EAAAjHyDOl2yvoHMAAADlTjFrpnwwc1kymHVYk3pCon6Qo3cNHRcyMzMiRTjcbENceW3jxavVhVhOWVZHTk3w39EqW0gzjGxNnX0xf2IZMilTmHw+QkFGp34EEAtdh3TFnXDYrYlYa2QUKCDQnHNbcmlUYl0mUEB/pXpbjXcePDBdgHEtZlG9nnG2n3LduZqgonX69fDhwKJVp37VqILTonrbs5Fhp3zr0777ybTozbXz59uooXR2pnuIpXmpjG/uNitiTUX27eW8Tzj2lnmYo3b+7+frLifyZkn91cTt2sjhXjr94tX0f2HTil75rpXRkGOPVkXmRS3zc1XYeU3xWD7nOSn4oob6vKX1i23vSDPgZj/bc0jjVzXWf1OEWUrttprtrpGdV0JGPTrcvJzUPy2ul3Eqd82xAAAAEHRSTlMAgL9A7xBgz58g3zBwr49QpEM33AAAKv1JREFUeNrcmF2LqlAUht3al30u5AXFLSheGglJ0IURTQRdzE3N3fz/P3Iqt7Ntjp7tdJiynkvx6mG971qqPYKB0WFM1/W+9Y3Tsx5jhtHWXh7TYGMhQEFXZx1De03MCdNb1g/pjtiL+RgMR6UWFvY123If48lrBMYUGooCdv6H6+6plHf36PtSitShPTfG+KoY3mx/5VI99q6/s60io6GpPSmTXqs4Cr47JwWbv2dktdsWh+MZbRgFD4vDxzvVYJNQGfPj5/ZpbZhM5mLrv1NNYgQzogoduzeZlI72JHR0K+ewmlN9HAAxnUl5WaHK4WiNn2A02qxf4UENToR0Yurwii71v2ZDb/hKMXuWwJYeahMASOnEEkJF6WwsrIx+p7nnhqHnS/NzT7cA4FwWMwgVFRwPeU5YM2V8ibBXdCOZCeKVKmRQFs2V8SVityf6PRWS1baZMgZCxMKvbIhkvfHUK4RLFUpcu3kyzJ5CRJZ/BMrerKPCK8g4iAJtyKHRZi0popIQQKpWkQoVabVTJybi+bbdCxndJnzLT/oKERnREpipVYRCRUgVcAdIU8CZfYvJyNQeiylK4nNOStT5X6tVzHAiAJDImGyzyhhqj4SJu3JPt8PTQopilQqKgIgCOBuSrBZZSgbaoxh0s4PKpdvxlkAkVQTq2gw5kRdfh23uWxfGD9olTJQE/QdeAsCZqlSo2dvZLrl7fcqRsGtnYxoGDpCk30w4MbDMswLnRhUyJUy7N8NsJI5UlzUEnCTL8yaIL8+Eg1oqeEQlzA+PaIy2ntXlnOoSQVBsxA0QE3kOlj9SEWJDpRwXl1Vyz4PLaNUciWkoayAnKlxLAAJ+Hhgvex3w1Cpm4vXqwejdrT1Z/ZYA1t61CodyUkc8SYA44px7BPBMxfSfA5YoGuNeIWmPrDN+3T8QycUFT6aRA/FDYhOGywClCBUVQ/aHV6vrbROGog9bt+5TfsCaNQxzEBLIBDclVVBLQ1m1t75Me9v//yOzr3FMgglpAjtSxUerEg73nHt8CReFJyGOtZJnEMncMy7bOe5OzBIgAWuSEiISxDNgbWVYULCQ0imM0gOMLWb+ztRJ3Dbx7VS/pBEx7pAX3iEYRZSXe2xoMXG38YKQ5DY/pa1+mdkw3ukVBzoVwACRlHAQRGFvm7C6443SKZpIyUYdCOYShydRUkUJQSN4eQbDmJWLaxDH42sDJTO3n2dKHaSsjjZL4TRNBv+hrsnI3MN2krfWPGdi4gW9DnlHD5egIr1kMmIYb+eK4VdgmM+DNjHYALXdFRxdBjAeIk5M5Y8gZUhbMzAxEjArIuhAOiSiytAkGA4dvGSM1Xz3y+93LRczMfHr+Ick+QAVaDJEB6GjKU1yBYAT5do8gYvPMzHxY8wTSOYWyFxU0NJjLROE2fZUdNLW9cRMfII4MdI6pLk7MzGZmIrI8s11gqMEtrSpSXf1ezsDFx/GmLBD2MhRLdMKREJkVpRMb8waLa+YVyM0DxegjnEmjGR7dgGzmcmQa0OIqFnkVOYk770pMVx8/U9MFHW+552FIyZyNB3q1h2blphMa9DBt+Xi3eRMUC7KPhWQh23MPpRIOZ0+bLxQYDnMyC0/aG4uLBPAPXU9pYIaIfe6CId4OC2o8AAiI+3FcnWmQT00k3JxbdVRuq7nF4qL1jQVaronGCiKqZGzwxBOHIz7pNzVxaeJmPhhnjARjtRLOfEEolrExO+trHM0AxrNO+90Ftav1wg08nOStdm7TsakVT28+qxdJcH0p50HyjKsT2W96mt0QZrc+fHqwkkNrDt2TdEDsnPXNAFQ+LaEuSCt008NWrVTYdEtw+LQx3wIW5aL95eGTLMWtU4ReVV/PtMVrk/bqSYroxPEsY5j9Br4RGWoiu1UUhuF+HuEEZiQdNap15c2j7tbq3vC24SXOXRbwGUreBTjuE/wA+yEC4zXsBfjNHmI18sT5qW80NkqK/SVHa8MSo90JiR/Llyafdmb1FAzgasPyr7SBmb7aXO8CBJVBRjjVB/LvQT2HrBGPLpM90pimnZkBr/UUtGebxpk8Utx8eai9x2PA7MYQW2T7zaOZixHYIklCtUGjhO8UCdgL13H9+YoDIJgaNlXIQ7Fb1o41OkeFX6rG4tn1UauzrdM90TX6MHaRJEh6kclvBDjfoaOIIHnHigCbuThEuOnBd4giRQ2W4wDUIvE4j502aZ6CizaHeqpJ+9SkZHeAuD2fOu8Upb5bWiQFMHDBwNpS6Q8ahI3m60xCXWLYYATbRFPGIcbeQLqZbMEDgL9V6n6CdE4qt6b6cIxRXo5e2Wm3hD/vHU2dMW4fhKVVurIF8/Cp5XxA7j3FY63OEnBFFbSPQNZGUCFRgq1kmpaRruLnWFZjxLONPMH7OLMbw28DIxtI2kJxW6k7+9GB840tQU7fFgjgLrvDV7FONkodm6wUopkB36Rqr1NCEURaDWtTv7yjm2dfOBd4u+z7OLTkfkd8yLpVcz00EwzETmvDlUg73xpSwTj7UIWxn2sbnODWwSKiiBM8WIJtZK2/RWDvOI4vkFHwSFG0LZGCoocgAT+5bVUvG+Nwu2a6rJCT5n/kXKtTU0DUXR8i89kYkKlTSClNaGNbWiwmkhbR6r4RhjHQUFQZBRR8fHJn+8+s0k3u0nbM2N9Aznce+65d++WVBJx12WBiEBJQsMCpAckoGYClVBsPTIB4L8J4O9NnEm6HlJhUShfjlw2mh3SIldKJDx4zE3gLq5BR7EupIIf6HZo/84jAqLYA0/jxyUk8iAV4MUMQIgo+GE9HAEOSg2kETQ6QlhKXKgh+YLRAfkrOV+ehykyM3Yd3ZTc2+B4b9HqyqOH5cAHD0dNJXz0AYiIQc3BaurVamao2zAXUFi4uk90JUQaYqFECwpw0aoK5lnMXZweNz1eS0ZqVCAY4KJIChaTBx8Q0UUqSO2lYtpOWxnNfq+NmPL1Nvw3kaeYyG5FyICwSJFvOWHFEOL5yngpcgq3HiJUsL2Rw0c5jxCSZ7F6xGLkFkgmpwNIQYT/yK0VMxjSFnBrrBSZwekxHbpEBbChDHFNtZTCCHyYVD3AXDIxCKNCtLigmC5FTqP0mBKWzSyBA2QPPZmnjAHPxLGEOGUwgSkTU3pXdtmCmc7rBQdXuHpMh5RTDFA/AeRiIvgoQcI2ZjhEJkS+HowLvLSKnCvce2wpE6M9iB/BthARPRAhblA8OXiP5tRAYLVxSEBYEnuBrFa/DNFUhEbrWlHNfKhMjMAGGU6KKOw0TeCdrcBTpgAKBEAFDokwkhuMBzcYSpkLPJswRWYKauYcHdUIIZd+xyPteA/+NrREaTu3Oj8/P5wFmAeYm1sWtbWOE1ooJGwTRFtYbHG4JCons8WU8yLWzGal3BlvWM10wSaP7+mEFh7rm/OzIE55zA63+D1IphLYpAaIH1ukGOW7HdSyo236ERfWh/v267RFzfeZ6/ECVXU8mbCQ4A9qMJ4DFNm+ydOwOkyw8G+JIMnHO54O6LfsgQM7fN1DTKCfRWyglr0Sd6hsyFOim1qXihTSeRZm/co4TNg93GIALwDgmpZf43jYGnnDkj2N4En6rU+Gq8tcWOAJBirSli/14WXUL7LBFjui6MQTrfN5QUF9JmnBS/3mGFUvxIIPk9rPqHnLKR6MW0eLv24+0gi2/ywu3jpIsTGXxTcyn5aLbJs1UARADWMpmR90PB4X1EtFggKjWcZn1neLqmekO9hbQnWzanZvpEMesse8tfjnWIXY1Sg+qAg3948O2TvCbHFCCmehaDjeRYR0JVarxMSOzYNLlbignsoPCn4i0ihqAEj35XqCu8IQB79vqjG+azFOVIrj/SODhsa7dU6W27BMuTg0bEmGpH3nfXhG1WmQjF/N81mnM6b9ZdSTVwvkiYdTIsywQHM0Mw5/HatJfGRU/Ez9xZ+YjeFyKkF8G3LuWogJPZJOEirJPYT0Cm1OWJyBMalwwGeTLVmeWK4bBp6v1/CQoZcdEcbRFzWNrxrDBzWNnf2DrAveVi/AfsslnSsDd05YZjLBikkiLORBsSVedOmUxUzEw2oAO+2BlockIBZ31FF81hL4xv31zSNCRipUQ8pEhOppLRAubXXor5lssLCQFZHLsjFFoyTpcyyzFqHuAMlYNzWF3FrBROyrPN5oSXxWeRwDMrg7SWGAbEUQkkFxT7i01YhPe8tZm8+XZEZzXjoXIcVJyIeNuPD4+5/GLzUL21oS79+oWWT8xYGRDFfMBDxOQwpqCVKEqEWLm7rleYsZGhQPoMzy6OS7zzbigr/8+XtHzcKb91oK22ombkLNSN/H6UEmLPAiYoK6xL5SYXtzxCMAVIi3uCBuSYd0PV3hUBUFhTVwHDqLaLu6y6nELSKW8qBgYcFj0Ri9pRUEiA+HMGFaoioCnWJ/dFcHbX0uizuRs7D7oA+dPQcoZ0WCnzylsLpW4kYbyw15ULCwEOD4Fnd3D1VtwkRNjyTrro0Rm7XQZHsGF0XDq1nBhhdhuJU9uNMdBxpMKhHpWwgHJCTkQcHCQoRf/CVfl8i054qqaj99TNFEZuNBarQ3I6ikm0IqGqL5aReNlzwHEJJ1N+W3KsQJCwq+iPD4cjh6M4cw0bPhXMBuZytntcIcwWiDOZtts2aQvRJRUSmJVu1s3CCi0wru2NrYV8X4oWVgVxVi5xbi4jnTC9SHdCEjnputnomZXoPkRn49vUZSkafi9n1wP1Y4SKb9p5U+sRkiJrjkEBjNpOWU4Gjk7ppJlaoHa1co3VrqZG0LrmQOvy8h0aRU9NnYJ75w0hRQUaOZWxth4gCXULlm8sopwT7jgh3Tu12oUwOR02IikXQCEuG8njj7uB/7hwcLuMG7K7797JIs9diXwjEhTw95isi5QGMLC53ERUQ5RT1UtSI4EzmX4TRXWYNbTTT41aZsuagH542oC/GZTuQz8VMT4eMbVYI/RrI7IHnRhl+CZQpSo5W8TCNtRJipWGZzrz4KKkpmU2a50SDTSc6sVvOZ+KaJ8VKV4Uu6juCthIHuy5r1TrUpOUE9zefHMLFAsQAqD17Dk61ZeWYbFzM81iWYI0zIhUKC72pujrAtmAgZmkkPnNaRteDyY5M/Saiy3ZJKdjxEcMgddKOBF3/0lXwmPmhSfMrjgn3fsFBMDmiIr4zmx4rC0EoVjQfZomnS1Vv+Vo5xrMoAmJiGi8XUam2bOk05JBlyQVA/MPqJPX6RaAJXY7r4W+J5rHhI/QSb18jxTZXhKFVGaqEyOdZHa8i1mGZWgMjmoVA0AzhC6oGqTiWTnUfuT8uE9v6rKsHOAZVOvkueJEPOj/ir5THfFNHXu/GGupMSiiOpTrzUEKbKkWMjc+fa69I2oDjepfcXLyNNFoEMg/igCNApmO902U7lLJFMuWJOz8UfcsidJoJ9V4rjeXrce0W2UdHIFk0XflogFRbXg8mE4pukivJdqgS/R9euTQcRQXO1OFZSA5yrbIeZx0L2ybxV0yECbly4qIrxiWNChg8nErk4TKVIz9HxGmB37LAYphZPoCBnetbybXrkSsEHpJn+qAeS5OAEcwrxvImP/DE8RISHK7ynjIXVZDk9k7mGVsYb5NygQpydz+XpsftRGxvfxQ3J36TAObrrsaHB5OX0VJZUACIg6JVnCRlRUjN/FwyJ6QNjx2DKyYJhgCdIcvAt2XWRVNAWv3S/rFRQo95QxGR4id7D2BF1ojKVePRI1p19k5jOhywsIrKZ4yuTiQVz3TwaZWbDi6wizYo185M0N96+WJOXkhOxcq6ysIhqXRudpo6JVdadXs7dWG3Jt4VZUBzuCIiQYq3+bEmbgIz9OCzYJrTfHuNee7UcSxyb+s/nvctXuopYgqDIcNwn23kFdONp/dUdLQcvsxzXYdJn1eClLN5ryg8+m2lncQ19QCnKqRSBnUfN5F0brxRvPhWw2Uv1O6/qj/IF9DOnoPuZLrkXdoXRXUqM9Rbo93c2btRPixsQ7r8ld0qjQTspPpxS7G4XMNkbey+MZ/fWjCcbe28LsPHphCsi6ykRH6CuyBO7RXakHp8gzsfDXnYAIl+RbbJPGHRtnJndXtJo7tBg2P35vVDX9fbZnbphPDbWDOPO2p26VgAff2x/3WVFJGU5Q1/HCIRUkPCm7xDUgZfmN6nJmiFRJkdptDttD5DXrcUy/Gxp7yXCGOa6vvSs/ri+VH+xV6+vvXq1MYYpx59raY8VP1tHiHqhaC2pjy1jI/lUdxO6eUagmvK3MfIwE7oZt/0b2vh4dO8FCImnhlE3nu49qWsT4Fk8iIyAbA4cdMPKFo4bFiotxkWTjB/ou56cyrsHw5+YmSQS3bAdu9en2iTYePW0vmcAKtZARdUmwYt4zBnoPnxxYXyYIvXvoD2RB62F1m28mtok45vrZMI7p+SLBYsKn6iml+zOH2uTwTBePAU/nhh1bTLcW2E9mYdfItGhIVxbp6sGfSQXVXoV9xQpIEouGjSs6I4qHdew/JgMoHw8rtefrBnwA0yXIa4+gC8BDA3xXVTUXC00Ri/LXKW2OxdVNsnyIlo+AovWD1l+yPGft+vgbRqIwmKLITbHikMCNsEkkEJleswAxsa2TAwIRcwwBWKJIZD4+dyw/Z3D3SW0ggc0bZqW3te3741xRpMwJmEAh3OVEiILTrjOHFx0bCO1DjHh0LredgMC1jreNB8Sjfp7/dq3IPnRviggAfg7TzJCYzquBWRKSc60yGRR1bv3Bq4B+DWVxb+UpcktbVOyiEAErHbqgymgO1FhfQsF/HMoIbQYE0Ly6iBBmIcBczfjDC8hdF+Ml8yjpPayugjQTW1lnaO6FiqRsljYluq+RXtQh/zJAqI+Kfb53FpwKMYVk+Qezcd+QP1a76aEEKZNGedEiwiNR+vwdNi2MjZczVmSBWqbF2ma6zeK3K6K2QoX2w1UT6KyykBTQmIBxYQSGlVc4XsejZPAy2osc/Y6AQV7XRLNc7XegavtdNQyauSGgGIdbKlNyFqzEYiaGTgi0lfv58TiAf9thySdsPPjhEVCPZplJFSeKjge1PcIIf7chB8i9flQXLakWtaZoWi19NOYHd5p73Ina6kNY8Tj8wef5kPBqHE6JiweJSQJSKqKWDSRn7ND8ZhH7q/szgDOg0sMPRTNuPTMUaXNyjCl5IIsAFMnB9yt0rsfjV4lJfmUMg3hZ0Hhh1wdpCE7ZkhITLlfkSWE5vuiPKNcNMaTPPEmUUIC/s8Ymb2vLlCPLLh/oGPpwt3Z9LA6rJYP2hbTI0HCh6nSia5y71gmrOwc4adkXASkojhhx044MDH7w/1OfGrCZcbKGc8e1NHpFVR8OMuue6nXQ4sSDmeHYn0JBfI611Cr0tINbkR23a2hUK4/XlqgiDJCfXa2II9YPjMjQUA4CB7ng4CGRERlMTt4EVMmLyGh033QF38igQuRYxg0VZH791BsU5xNjCCTBaundVfoS2pJHgyI6SoQVqFICSlC2NFJRghhAhFS8RCzBwp3g1D+2hDW5k8kUJF0qwlFr7f0d1DcE1Cozma/XBLYlx2rInDRtRe7bfk4hAE5YanHjMbepPALylDwFM9pKgSB+xhZyFARbAHjyTiIi1LIrE3u+XokJCkncK46TpuzrXvgoiETx2fl693NEgpYzaOVeHCNoYNi0BW4d3EtBij0MsKOm+6LuF7wCfGQq+BMkXKxCLJMotL8dDoRbmnO3k7MSPxhTU8JKHravH0fJURGKIDcStk5d03KibHUH5XE92dy3Z81ikIKfUIoUv4VU4RU/ksSIg4NmfLYa+gE6kJf0Hhyxpr2WM2eo4FCgCAHn9igQCheDo7nSxfOm2oUl1m7/KXlhtI5bKmrEQfi5tNnZ6+9cMKJ4yHVZvkBTGdESFrwZ//kisfv91ihcBkUS4beHoyAsUNxnQHQR3n4/LYgQGGpO4vCrBBnDpVfrzx3Ur6JJS6SLcBMRcqf8IKppgrFCIXDLiacUz29vynpqAUKtGOqG9KOd/70VAeuIIfRUIXC3N8Q+ex0VMoHifA7L+WDK9M8ARRhHWcJ8ITF8X1tXSegWNVGPNBtQPEXtAzT3dNCATMC9ej7hBE/fq0JAAUdk1S8kwTiQwWKzONPc/7wVJW5Siiuz3IFaHVQdBeBAtXsMi73BOsrUGSEU1bhkaVgkimgCKakith1hYyA4pzSyMZoNNB73hW11gyFSkObgKilywXncXFmX4GCgCuE2mxICKDwxddxH0ztkjBzxaC0bt1lvao4fhadLWuGwq42dd5FTLNIsH4Bq+gTTmkg30o4qrcpGKeEIk+TAnZUCwWK5Za4r6krODkuTMdp3bDBc6uBAhZbDwXozqxrQfYheyUPHQQSgfE0BZPUqpWyF3hk1ql4t8cChVPGHg4b2KiFwrCD+cgqoeguBsXTOpygsYTCT7kOhCklARVckFUgsGdxdHFTFIuPi4Tm6JDQQYFJhdU7XS0UgGEtUAB48eA4V10Fim979Gwhw2wZmI/D+pwBtKbUFSkEhOT1S6aJgCKDknk6BwoXP+W/ggIkZrdI0sYgcC4gC744epLzN3UqV9UQmQqFJ5lC+qjVV5vbZgirBABX4NLwP0ABcsxQgC18dqRICsCEktL1JqqajAkFKPyhCtZC+cSE5zh0TIHIFN1bF8smkQtaKFo2KDYC00WhuOC6IyYgbdVX+64r1CyjDT/iUlIfLVehSIIgCKMi4KRAEQnJKGjpcxV+ZGy9/CBLyWBBLrn8+s7RVlfstkCBLNaitPTn/6KkbgzTWwoiaJLyoxVMasYe+8soDmrK2Ifieb9Uq15pZywl8MhiSRpaik3227NY2/4eil4FRfviULlGmNM2KLK3me/L0xNBSRBKQDhJGKSrlQpYxvtiYWLGiEhNzer31bEJzMG65Bg8LFtCbxVQdJ1q7uVIyZ1/sA8tyT15TklJ6DEKg5Q0iUrW4AhISGL20cTeC/AVGe+5W7pWbFCY6/PsJIcc2hwLNRKZ4swpP2yWlien0rLE0sGooGIIxBklFQWFIfpACAJbP7o6NCX/zRbkroBi3aqgGJ5Sk8r3YEK03veESKIMhlCkKPKfPz89fPjwRU5IWHza877IU5JErx8+fP3j58+pwIXBUUGTlT73Hh3VVXXo83SNgemh/pzbsSurmLAKnmAmBHpTKyF+dbbU+8G5n0afqwkWecQsJLe7/L0yvHg85iIUegnjjkyJSj6aWnDvwv1DrnGxodu4yFlnukm3s4ToN+41Om3IHtCsaxFlQcyD09R/z1XpJK8iiY9RSr3JQxbH+hnxqnlpz6KxCLs+cWPrjbMgR/hh1ZpLLCJ1HL5r4aKOK8yTreVmsg0ofVyQRmKI5hCuLW4KLYNLPJ7If8GdjSIh+Z2XDx9+/PQzF0Yz/BELIzv98YNLzcuIF2bd4S/lUQtUr74tBG0Lw2pIdg8RyOxuIdZYbSwq+Ht3k4/aca86ahx836QsngIKce/HuGJM5W0YMVASJEKVMq4QN4tzetUJrrRGB07VmAy0zub5FrMjp/Ue1nZjMa/YAqNdEeXWEnnBhbIwdEoBCmk9fgRkMaLxj1i81j7Z4jBUBSIQdivU1hsQUeetH9ZrqtA7c/x8NeD28h929Kp7qRoUh7IbeBb6rMXEXyUhU2HyKq6ow7JkMNbVL2OSdWlntOOsdT1CrevVqn/eMGGioQBkWWlc/WJtNV47aXtC3givAsOYuAfsdA3BWKezX2tEbskKvaZjwRf7l9xw7foCtuSC0gLxyqo3104W+QBbSOpqVYVxR/nBsmVqV21NgUNnsV3e7aVu2yoh0Jtrp2f6Rn3IByae65qmWC3z+UOda2dMVWnr1H6pCocVFDaaCAXmVxVx+2oZX7J2umPoKoTXXa3a+du5HphlzW+iparssAv3GbHon17RAdkeuaM248dT6rcjFhOydvpo6B67tXsNBFu6tdqLYqsDhglpFhacqoYfoXXsX0LxcE7z2OKsYDAgltL/fmlIVrTT7IVUjpq2+c1/huKw4hO16w0Ly6eMA7IscallTsH1jigM5+6FzuHsiV1Qs2Wgh42Oxdrpsy4+h9Ic1Xs3epoIpHWNzbzvdI6fhrTrJhZvgMApK3yFd9VCmniGKy7IOob2cu+qUj5/8h9C8dTEFJIGsl1naYSIoBGU1tS5dr2PBbFCzaETe9b17othcSvXKyftmm4uWJsn2Hk33SUV3G//BwowxbE/1t5Bl4P6ouL/6NHrx1fKxeIdWV+EHRkYL3m7XG7MHMwVOd8ervt5Xa53eeBKG35R7ap48x+h+Ia5acoyRG2K93TdSIhNxHVN1qNKa6J7TH7FbMvECm4PmvOjy/Y9obrBFl/+n65ASzpIbsLUV6TNhppVn+lNTLsR/uYjOKfN2eWXtb35AymXAwbEpaW2whbkyf+yICcambyRosb0s5tN9uOcuqxvU/VNRWFe/89EcUeX974wrGIgV/Utvv8nKD4QwctYZ1WP/XQ03eeWXX33Ggvlt0hlsYI9lA0NCuYCDdrygXNkd6CONTn8f7zNk2rKya3X0wwuLOlz3fpgAqpCUp3qPdPXbD7gdM0YhAzEMix821kRefqPYpAvYiMYxPWS3Npj2ZtiIiz2NA+7aa3IqNaUG12WrpbYQ6rusfr+HyLTt0T1C/nOqUFPU3fUb2HwnS0W22GaJoius0OX2T8MNZlthhAoyJ9EjWye/8N8BSbHQTzkT9ETRu3UsLEc5bx58J06ymtrcyH6PU3X2RkuZoZxUC7fsMZW97JyUcWUPxJTkP5JFuv9zDzB27Dsp+RIv2WsWsKgewy+O2oKQLbNjDN/pLltFUblzyFhMB2DrthXrDSi3MScl0Zd79rpXXO8+RV1+ujwN3dn2ts0EIRhIXFIERLCUpWSOg4JoYIcdi4SaBqaRCmHKqCBcoPKqXKUcn0Bfj3eXcfvOns4Wgokng8QLtV9mJ2Z3R3P6/lPg2yGjtUGgmZZNqSmDpnsSDoV6gnqXBgdJ45WdNjkhiBcYfLoISYQMYWcic4edYKuWoc6anTn4dp4+quyomBdGPyPs97+1dJKWLaTiszFQEFx4GaNVJwV8SDk3YxRE5MoXsT/RT5kDuETV+jIn/xSMztdc9v9oFhaCUQLbLlINjhghQQStvxUWteG6MiUZZm6GO0wL4LHXbDQRs1bHz89O4hkm71H+zoiOyCBvUeTxEwvW1lTjFWsToJmgyER1wcmefMrhEF0J0wLhVIZ0mVSgedmeLiKQ4IRWDxXYfikGte9s/dahWMvnIaFEaw0WrEx1nK9VJcFTduVNjSvY4Mu5pCS2+iKGlPyzFyhNU2TvjUkZbEj/Zb2X+6c1tr7j1Iarydp1MpE919FJlqTdRQ3pTZTX5PEijTWB1YIF4nOLvMvmJY7/gglTR9TkyVzjxuAWh9SFtL3C1980nHAYpFk4Sd0dUzvR9u49pBYl70atCy/C7qO9REVdG3VJSN+SkRXAh0rspzK1FF64aEvWOwLCwMZMc52xNliewIJ5pOo/KXbjwL1apCA5ZA/pJPuy/y37dK3+xX5lBGoOWFCcaIs3kyDQIAwgrE/RcLrOWTSfVGtfLDCHeSV5fuPlER+DRmq2ueCTpXRVUhBhK8xOmt0vnnIgmSpa9GlARAzw4i+2b15OULCIZFyqUYF5h21VCFzib70AkSulZKSjJrss/4+nVv0mHd6NRK9Kr3InextfqE/Om1iB7d4FDeQO9DdUGsXEbPFM02MthFspFDQOSk5+GZVCSKxsqW1mSfFt0MG7/AL8TzWxoEJiOikzgc3rAFIeL4P9vzJKoFVVHO3Ib0pBk2F8O8xS5ilV/J5xjaLe3612w7EKv2f2xHRsUubOJg0tYPnkxk4vPSYl2ehyWm2azjlFlWxECPkQfP4bGJ0HQ6FDRSS3iyaRQQWW0Pr8kU2XcHQoPS5eY3VwyCBRjxvY62iasnr6MdWp5QShYOpTATRmaqk4ERmb5IHugIWnAbbtc07UEMwsqdffmxevGm1xhES+QtgIbeSslETleaRGdVM+faCQkcaeljy8MLJ94wFbNyybuK808g++yn05kRyDCQ8R80C4U3TLzMQ1HNgpwS3EK8eu1NX7WejZadDfowUf/W09UcodgP53zFf8DOZsRgWHZTIMhuj0pRYKqaztzrV09VfDv2v2WYsNoSel9wfoHhKQDCLHKq26dcAi9n7mGFpZFKlNp/e4xo2vhgCaYUGTnaGAhJ/iOLDF6b5OxjwKBwyLJD5HWOhediurr8EJ90qt8jEKPS7nI90p1ZHGzeWpijgEKuh3G+aQ+FdwNfQaWK7WB5SS2P7YeAWZPEBtd0IEhVYZPPcU/YcBYrdr/H+sPsN8uDssdHbn8/D89TaKBjQL7UMyiuTaEFcAsNtXawVbBEJicqVYJvmyVGsEpl0/I5cmJFZmj0LUBTJiUBvCWeYmghP78FNnAJuMdDs8mx3clnmCpveZuC5RDSEBnklCmqvvp3bPSPtkT8TOMREIh0ommzLt6GMEKKqubzdcAynMHALioIgoAp1HUntmWWXUySJ4G5EhQL2ZnV1dVfophkhe3JesVZEUau2qg0hOozu5m0Ap9C6RauufPmqhLZYie85/rroESRMGzsWBexcFAWCdxQFoqVWYavQKKO1Dn0zsLsxToFb9ZxaVKhUJY6n1Pyr0EsZpwYPlqJI1zO57aERCnRLZjVZf7nPNVwKj1tvcTWFXkHfuq6qsHyjn9Q1OFFB9lBxKlAEu+TMOJfLjQxQkLk+nnac/9lpUdcVzER8rCw0xbPvbQXtUqHQj3+trIb/Mg0KmAEK5Gp1VVGwo52XfvK30QZyVOMUUP7Fk5hYNo/e1r+FIt4KSKVIJ0H6T2NLGmNHoGNlZJULOIH+1yhg1alJvIFI7WRznjo2C4pjKUROMys6/x9FMJ+5U+bbRUrdMouZouKxarOOE19DmwcUweWga0/KTzZLdFtIpHGRc7j4KIhGjlBjZYSYqY+cR8kSSQAKpt/Ma2HXBzPHTFwEWFtJQBFUFQX0U0B8Ls5wzDlMBgoWP8vYhqFhdfYl8jghKEjx7aLinn15YFtmjROCgsBAcaVYHvos0qonBQUexTqqWh76QiudKBQZC527sxq2qI8ThKLe0hxyx+9FrHFyUAy1e4/4jNraSgqKEZ9HjcLFoJ4MFOumgQJ361Y6ESgylnByZVBdjBKAYqtFKwqzQIH2E+vuwqOoD4X9qFnotNYXHAUjgZBpGDpP0JS62Ci2LXRdmRk2Zq2tRUYxsoRNmGEaYSwWF8XIPHmIFThlsagoZCTMUypjsZgoRlaQRueAxSGhmA8SYGGM4ufb0AiKX8XAmg5D8d3/g4cEw0P/w/eBj6JSDI2imBMSYGGK4uFSaATFvfBX+SxFQT7eJyi+k0/3fBRZ/AuKYl5ITFhkDg0FWDgUBTGC4r4KxdyQCFhY64eFArYRovhNzf20Jg6EcRxPNdEY0xQfkEBAxJN7WwOysItQEgI5eKmBRXwBXvZaUPru90mm89PLwnb+JPFLqT14+jDzZCKNH0wxN0eRxpCwYlEpU/w+iiTFgvvJr2tQ8LD4uKf4thCpUaQ5JCxZFMoUM5GkwDNwoDjz1LynWOtcQfZbSFjoxSUuTs1Q4LsQQcHvOJuiOAgJ34YEzuCU781RvN5WxWp+babm1QRFRThjWmo6Jm57MEaxvs0KVpjNrvyqT5EWpPDxndI9Oy3VryCg4LPTr039+knxXs9NlgCF7MsUpRiY7sixm08YGDoUCOeKC/8cWUSTAmPCnTq2CwmnLT0KfGGqoKgZmOOsS3EiXDqsN3SxSbQp+L/BJcUf3hz1JtGj2OcYmG00EQMjL7XGpniyYwGK2YrfwyJaY7PaYky0VCQ2SaVDIb59bgMKcfuxmmlQlDE1BVOnpXDaorjUoWgeKnoFxVkcs1QosCRwrmqxyQALQ5lCLAtQXObcuwIFlgQ2R8uF1JQn6hTiWTNJ0YzWiyrFkkSDidNuOHpyRapMsbi7MxU36Ec1iiSjJjd0uumJsEvUD97z75LiWv9WoSjjLpcEJkZTlihQyGXxg4gkwlWBIj0RlkSXjVxqihMFCm7Df0mKM7/pyxTpckuiZ8/pNi8iYHTw4f9OQgRDp/umA2C0TJHusn7sjVsvATDao8DW4CLP6U0jiZHvWqIoT4DwJ06venJJlC1T+xTJG/UVgvOAQUVilSKtsj5DyG2CpVHaoji8Ue8hGowByfIqNU+xL7Ykc6MeQ9QNfUJxVZqkSE4ZoWDUo6vGv/LCgFC+LMxQ7Or1gPw+HKj+c2m4hIxQ3DcOH2BB3PJGz3YogmjqPFzQ0Kd4aAep4bvGKMbhwzp8Fpmh8B9qPvxlhw6NAABhAIjR/ZfGvUJiepeMkLf5UzFnPxUqVKhQoUKFChUqoiIqoiIqoiIqoiIqoiIqoiIqoiIqoiIqoiIqoiIqoiIqbjt0cJpAEABQ1BhISDrwZC2CxXiz/5u3z84y1wVx3ivhRUVUREVUREVUREVUREVUREVUREVUREUOqRipUKFCxexiZ+GKPRUqVKhQoUKFChUqoiIqoiIqoiIqoiIqckTF7bo1q7hfN24fXDGaVYxUqFin4us8+p1WnHd+Tgv4nlacVqRChQoVKlSoUKFCRVRERVRERVRERVTkkIrHZeO5dMVIhQoV+ft/j4oXDp6rE0KBWrIAAAAASUVORK5CYII=",
                  width: 30 , margin: [30, 20, 0, 0]
                },
                {
                  image:
                    "data:image/png;base64," +
                    base64_encode("./uploads/" + player.img.split("/uploads/")[1]),
                    maxWidth: 100,margin:  [70, -55, 0, 0]
                }
            
              ],
              fillColor: "#F5F5F5",
            },
            {
              table: {
                body: [
                  [
                    {
                      text: "Date naissance:",
                      bold: true,
                      fillColor: "#EBEBEB",
                    },
                    {
                      text: moment(player.date_naiss).format("MM/DD/YYYY"),
                      fontSize: 8,
                    },
                  ],
                  [
                    { text: "Poste:", bold: true },
                    {
                      text: getPost(player.post).toUpperCase(),
                      fontSize: 7,
                    },
                  ],
                  [
                    { text: "Latéralité:", bold: true, fillColor: "#EBEBEB" },
                    {
                      text: player.pied.toUpperCase(),
                      absolutePosition: { x: 237, y: 110 },
                      fontSize: 8,
                    },
                  ],
                  [
                    { text: "Nationalité:", bold: true },
                    {
                      text: player.nationalites,
                      fontSize: 8,
                    },
                  ],
                  [
                    { text: "Club:", bold: true, fillColor: "#EBEBEB" },
                    {
                      text: [null, 0].includes(player.club_id)
                        ? player.club
                        : player.club_slug,
                      fontSize: 8,
                    },
                  ],
                  [
                    { text: "Poids - taille:", bold: true },
                    {
                      text:
                        (player.weight != null ? player.weight + "kg" : "") +
                        " - " +
                        (player.height != null ? player.height + "cm" : ""),
                      fontSize: 8,
                      bold: true,
                    },
                  ],
                  [
                    { text: "Note du stage:", bold: true, fillColor: "#eee" },
                    {
                      svg: `
                            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                 viewBox="0 0 841.9 595.3" style="enable-background:new 0 0 841.9 595.3;" xml:space="preserve">
                            <style type="text/css">
                                .defaultFill{fill:none;stroke:#BA9913;stroke-width:8;stroke-miterlimit:10;}
                                .fill{fill:#BA9913;stroke:#BA9913;stroke-width:8;stroke-miterlimit:10;}
                            </style>
                            <polygon  class="${
                              player.note >= 1 ? "fill" : "defaultFill"
                            }" points="244.3,215 256,238.9 282.4,242.7 263.3,261.3 267.8,287.5 244.3,275.1 220.7,287.5 225.2,261.3
                                206.1,242.7 232.5,238.9 "/>
                            <polygon class="${
                              player.note >= 2 ? "fill" : "defaultFill"
                            }" points="342.2,215 354,238.9 380.3,242.7 361.3,261.3 365.8,287.5 342.2,275.1 318.6,287.5 323.1,261.3
                                304.1,242.7 330.4,238.9 "/>
                            <polygon class="${
                              player.note >= 3 ? "fill" : "defaultFill"
                            }" points="440.1,215 451.9,238.9 478.2,242.7 459.2,261.3 463.7,287.5 440.1,275.1 416.5,287.5 421,261.3
                                402,242.7 428.3,238.9 "/>
                            <polygon class="${
                              player.note >= 4 ? "fill" : "defaultFill"
                            }" points="539.5,215 551.3,238.9 577.6,242.7 558.5,261.3 563,287.5 539.5,275.1 515.9,287.5 520.4,261.3
                                501.4,242.7 527.7,238.9 "/>
                            <polygon class="${
                              player.note >= 5 ? "fill" : "defaultFill"
                            }" points="638.9,215 650.7,238.9 677,242.7 657.9,261.3 662.4,287.5 638.9,275.1 615.3,287.5 619.8,261.3
                                600.7,242.7 627.1,238.9 "/>
                            </svg>`,
                      fit: [100, 100],
                      absolutePosition: {
                        x: 270,
                        y: 168
                      },
                      fontSize: 8,
                      bold: true,
                    },
                  ],
                ],
                widths: ["auto", "*"],
                layout: {
                  defaultBorder: false,
                },
              },
              fillColor: "#F5F5F5",
            },

            {
              stack: [
                {
                  image:
                    "data:image/png;base64," +
                    base64_encode("./images/pdf/icons/stade.png"),
                  width: 170,
                },
                {
                  image:
                    "data:image/png;base64," +
                    base64_encode("./images/pdf/player/post_player.png"),
                  width: 12,
                  absolutePosition: {
                    x: 550,
                    y: 155,
                  },
                },
                {
                  text: "ATTAQUANT",
                  margin: [20, -16, 0, 0],
                  fontSize: 8,
                },
              ],
              fillColor: "#F5F5F5",
            },
          ],
        ],
      },

      fontSize: 10,
      layout: "noBorders",
    },
    // ! Biométri Section

    // ! Biométri Title
    {
      table: {
        widths: [200, "*"],
        body: [
          [
            {
              text: "Biométrie",
              fontSize: 14,
              margin: [40, 2, 0, 2],
              fillColor: "#ebebec",
            },
            { text: "", margin: [40, 2, 0, 2], fillColor: "#ebebec" },
          ],
        ],
      },
      layout: "noBorders",
    },
    // ! Biométri Table
    biometrie.imc.length > 0
      ? {
          table: {
            widths: ["*", "*", "*", "*"],
            headerRows: 1,
            body: [...[biometrie.head], ...biometrie.body, ...[biometrie.imc]],
          },
          lfontSize: 10,
          margin: 10,
          layout: {
            fillColor: function (i, node) {
              switch (i) {
                case 2:
                  return "#F7F7F7";
                case 3:
                  return "#F0F0F0";
                case 4:
                  return "#F7F7F7";
              }
            },
          },
        }
      : { text: "" },
    // Bilan de Stage Title
    {
      table: {
        widths: [200, "*"],
        body: [
          [
            {
              text: "Bilan de Stage",
              fontSize: 14,
              margin: [40, 2, 0, 2],
              fillColor: "#ebebec",
            },
            { text: "", margin: [40, 2, 0, 2], fillColor: "#ebebec" },
          ],
        ],
      },
      layout: "noBorders",
    },
  ];

  // ! Bilan de Stage Section
  if (player.comments.length > 0) {
    if (Number(req.params.option) == 1) {
      let comments = player?.comments.filter((e) => e.club == 0);
      if (comments.length > 0) {
        contents = contents.concat([
          {
            table: {
              headerRows: 1,
              body: comments.map((e) => {
                return [
                  {
                    text: [
                      {
                        text: `${moment(e.date_at).format("YYYY-MM-DD")}`,
                        color: "#a0a0a0",
                        bold: true,
                        fontSize: 12,
                      },
                      {
                        text: e.content.split(".").join(".\n"),
                        fontSize: 12,
                      },
                    ],
                    margin: 5,
                  },
                ];
              }),
            },
            layout: "noBorders",
            absolutePosition: { x: 10, y: 376 },
            fontSize: 10,
          },
        ]);
      }
    }
    if (Number(req.params.option) == 0) {
      let comments_ = player?.comments.filter((e) => e.club == 1);
      if (comments_.length > 0) {
        contents = contents.concat([
          {
            table: {
              headerRows: 1,
              body: comments_.map((e) => {
                return [
                  {
                    text: [
                      {
                        text: `${moment(e.date_at).format("YYYY-MM-DD")}`,
                        color: "#a0a0a0",
                        bold: true,
                        fontSize: 12,
                      },
                      {
                        text: e.content.split(".").join(".\n"),
                        fontSize: 12,
                      },
                    ],
                    margin: 5,
                  },
                ];
              }),
            },
            layout: "noBorders",
            absolutePosition: { x: 10, y: 376 },
            fontSize: 10,
          },
        ]);
      }
    }
  }

  // ! Blessures Section
  if (req.params.stageId != undefined && player.blessures.length > 0) {
    contents = contents.concat([
      // ! Blessures Title
      {
        table: {
          widths: [200, "*"],
          body: [
            [
              {
                text: "Blessures",
                fontSize: 14,
                margin: [40, 2, 0, 2],
                fillColor: "#ebebec",
              },
              { text: "", margin: [40, 2, 0, 2], fillColor: "#ebebec" },
            ],
          ],
        },
        layout: "noBorders",
      },
      // ! Blessures Section
      {
        fontSize: 10,
        margin: [6.5, 20, 20, 10],
        table: {
          widths: [120, 110, 110, 110, 90],
          body: [
            [
              {
                text: "Date",
                style: "thead",
              },
              {
                text: "Type",
                style: "thead",
              },
              {
                text: "Localisation",
                style: "thead",
              },
              {
                text: "Gravité",
                style: "thead",
              },
              {
                text: "indisponibilité",
                style: "thead",
              },
            ],
            ...player.blessures.map((injury) => [
              { text: injury.date_blessure, bold: true },
              { text: injury.type_blessure, bold: true },
              { text: injury.localisation, bold: true },
              {
                text:
                  injury.gravity != "" && injury.gravity != null
                    ? gravities[injury.gravity - 1]
                    : "Inconnu",
                bold: true,
              },
              { text: getDaysAbsence(+injury.date_retour_prevue), bold: true },
            ]),
          ],
        },
      },
    ]);
  }

  //! Statistiques du stage
  if (req.params.stageId != undefined) {
    contents = contents.concat([
      // ! Statistiques du stage Title
      {
        pageBreak: "before",
        table: {
          widths: [200, "*"],
          body: [
            [
              {
                text: "Statistiques du stage",
                fontSize: 14,
                margin: [40, 2, 0, 2],
                fillColor: "#ebebec",
              },
              { text: "", margin: [40, 2, 0, 2], fillColor: "#ebebec" },
            ],
          ],
        },
        layout: "noBorders",
      },
      // ! Entrainements Title
      {
        layout: "noBorders",
        table: {
          widths: ["auto", "*"],
          body: [
            [
              { svg: svg.chart, fit: [15, 15] },
              {
                text: "Entrainements : ",
              },
            ],
          ],
        },
      },
      // ! charts
      {
        layout: "noBorders",
        table: {
          widths: ["*", "*"],
          body: [
            [
              {
                image: tags,
                maxWidth: 300,
                height: 140,
              },
              {
                image: entrainements,
                maxWidth: 300,
                height: 140,
              },
            ],
          ],
          margin: 10,
        },
      },
    ]);
    if (player.entrainements.length > 0)
      contents = contents.concat([
        // ! Entrainements table
        {
          style: "tbodyE",
          margin: [6.5, 20, 20, 10],
          table: {
            widths: [80, 30, 50, 90, 150, 40, 85],
            body: _entrainements,
          },
        },
      ]);
  } else {
  }
  
  // ! Statistique charge de travail et Gps Section
  if (req.params.stageId != undefined && player.gps.length > 0) {
    contents = contents.concat([
      // ! Statistique charge de travail et Gps Title
      {
        pageBreak: "before",
        table: {
          widths: [200, "*"],
          body: [
            [
              {
                text: "STATISTIQUE Charge de travail et GPS :",
                fontSize: 14,
                margin: [40, 2, 0, 2],
                fillColor: "#ebebec",
              },
              { text: "", margin: [40, 2, 0, 2], fillColor: "#ebebec" },
            ],
          ],
        },
        layout: "noBorders",
      },
      // ! Vue graphique title
      {
        layout: "noBorders",
        table: {
          widths: ["auto", "*"],
          body: [
            [
              {
                svg: svg.chart,
                fit: [15, 15],
              },
              {
                text: "Vue graphique : ",
              },
            ],
          ],
        },
      },
      // ! charts
      {
        layout: "noBorders",
        table: {
          widths: ["auto", "auto"],
          body: [
            [
              { text: "TRAINING - GAME", style: "tbody", bold: true },
              {
                text: "TRAINING LOAD\n(RPE x Durée)",
                style: "tbody",
                bold: true,
              },
            ],
            [
              {
                image: gpsTotal,
                maxWidth: 250,
                height: 140,
              },
              {
                image: gpsTraining,
                maxWidth: 250,
                height: 140,
              },
            ],
            [{ text: "TIME (MIN)", style: "tbody", bold: true }, ""],
          ],
          margin: 10,
        },
      },
    ]);
    // ! Statistique charge de travail et Gps table
    if (player.gps.length > 0) {
      contents = contents.concat([
        {
          style: "tbodyE",
          margin: [6.5, 20, 20, 10],
          fontSize: 6,
          table: {
            widths: Array(gps.columns.length).fill("*"),
            body: [
              gps.columns,
              ...gps.body
                .sort((a, b) => {
                  const aDate = new Date(a[0].split(" ")[1]);
                  const bDate = new Date(b[0].split(" ")[1]);
                  return aDate - bDate;
                })
                .map((arr$) =>
                  arr$.map((num$) =>
                    isNaN(num$) ? num$ : Math.floor(num$ * 100) / 100
                  )
                ),
            ],
          },
        },
      ]);
    }
  }

  // if (req.params.stageId == undefined) {
  //   contents = contents.concat([
  //     {
  //       image: techniques,
  //       maxWidth: 300,
  //       height: 140,

  //     },
  //     {
  //       image: tactiques,
  //       maxWidth: 300,
  //       height: 140,
  //     },
  //     {
  //       image: mentales,
  //       maxWidth: 300,
  //       height: 140,
  //     },
  //     {
  //       image: athletiques,
  //       maxWidth: 300,
  //       height: 140,
  //     },
  //   ]);
  // }

  // ! Matches Section

  if (req.params.stageId != undefined) {
    contents = contents.concat([
      // ! Matches Title
      {
        pageBreak: "before",
        table: {
          widths: [200, "*"],
          body: [
            [
              {
                text: "Matches :",
                fontSize: 14,
                margin: [40, 2, 0, 2],
                fillColor: "#ebebec",
              },
              { text: "", margin: [40, 2, 0, 2], fillColor: "#ebebec" },
            ],
          ],
        },
        layout: "noBorders",
      },
      // ! Matches Table
      player.playerTiming != null
        ? {
            table: {
              headerRows: 1,
              widths: [200, 200, 200],
              margin: [6.5, 20, 20, 10],
              fontSize: 10,
              body: [
                [
                  [
                    {
                      text: "Matchs joués\n",
                      fontSize: 10,
                      alignment: "center",
                    },
                    {
                      text:
                        (playerPlayMatch > 0
                          ? playerPlayMatch
                          : player.playerTiming.titulaire +
                            player.playerTiming.nontitulaire) +
                        "/" +
                        player.matchs.length,
                      bold: true,
                      color: "#0f81ab",
                      fontSize: 10,
                      alignment: "center",
                    },
                  ],
                  [
                    {
                      text: "Titulaires\n",
                      fontSize: 10,
                      alignment: "center",
                    },
                    {
                      text:
                        player.playerTiming.titulaire +
                        "/" +
                        player.matchs.length,
                      bold: true,
                      color: "#0f81ab",
                      fontSize: 10,
                      alignment: "center",
                    },
                  ],
                  [
                    {
                      text: "Temps de jeu moyen/match\n",
                      fontSize: 10,
                      alignment: "center",
                    },
                    {
                      text:
                        player.playerTiming.titulaire == null ||
                        player.playerTiming.titulaire == 0
                          ? 0
                          : (
                              player.playerTiming.minutes /
                              (playerPlayMatch > 0
                                ? playerPlayMatch
                                : player.playerTiming.titulaire +
                                  player.playerTiming.nontitulaire)
                            ).toFixed(2),
                      bold: true,
                      color: "#0f81ab",
                      fontSize: 10,
                      alignment: "center",
                    },
                  ],
                ],
                [
                  "",
                  [
                    {
                      text: "Temps de jeu total\n",
                      fontSize: 10,
                      alignment: "center",
                    },
                    {
                      text:
                        player.playerTiming.minutes == null ||
                        player.playerTiming.minutes == "null"
                          ? "0 min"
                          : player.playerTiming.minutes + " min",
                      bold: true,
                      color: "#0f81ab",
                      fontSize: 10,
                      alignment: "center",
                    },
                  ],
                  "",
                ],
              ],
            },
            layout: "noBorders",
            margin: 10,
          }
        : {
            text: "Aucun match n'était trouvé ",
            fontSize: 12,
          },
    ]);
  } else {
    contents = contents.concat([
      // ! Matches Title
      {
        pageBreak: "before",
        table: {
          widths: [200, "*"],
          body: [
            [
              {
                text: "Matches :",
                fontSize: 14,
                margin: [40, 2, 0, 2],
                fillColor: "#ebebec",
              },
              { text: "", margin: [40, 2, 0, 2], fillColor: "#ebebec" },
            ],
          ],
        },
        layout: "noBorders",
      },
      // ! Matches Table
      {
        table: {
          headerRows: 1,
          widths: [200, 200, 200],
          margin: [6.5, 20, 20, 10],
          body: [
            [
              [
                { text: "Matchs joués\n" },
                {
                  text:
                    player.playerTiming.titulaire +
                    player.playerTiming.nontitulaire +
                    "/" +
                    player.playerTiming.nombreMatch,
                  bold: true,
                  color: "#0f81ab",
                  fontSize: 12,
                },
              ],
              [
                { text: "Titulaires\n" },
                {
                  text:
                    player.playerTiming.titulaire +
                    "/" +
                    player.playerTiming.nombreMatch,
                  bold: true,
                  color: "#0f81ab",
                  fontSize: 12,
                },
              ],
              [
                { text: "Temps de jeu moyen/match\n" },
                {
                  text:
                    player.playerTiming.titulaire == null ||
                    player.playerTiming.titulaire == 0
                      ? 0
                      : (
                          player.playerTiming.minutes /
                          (player.playerTiming.titulaire +
                            player.playerTiming.nontitulaire)
                        ).toFixed(2),
                  bold: true,
                  color: "#0f81ab",
                  fontSize: 12,
                },
              ],
            ],
            [
              "",
              [
                { text: "Temps de jeu total\n" },
                {
                  text: player.playerTiming.minutes + "min",
                  bold: true,
                  color: "#0f81ab",
                  fontSize: 12,
                },
              ],
              "",
            ],
          ],
        },
        layout: "noBorders",
      },
      { text: "\n\nButs et passes décisives :", style: "header" },
      {
        style: "tableExample",
        table: {
          headerRows: 1,
          widths: [200, 200, 200],
          margin: [20],
          body: [
            [
              [
                { text: "Nombre de buts\n" },
                {
                  text: player.playerTiming.but,
                  bold: true,
                  color: "#0f81ab",
                  fontSize: 12,
                },
              ],
              [
                { text: "Nombre de passes décisives\n" },
                {
                  text: player.playerTiming.passe,
                  bold: true,
                  color: "#0f81ab",
                  fontSize: 12,
                },
              ],
              [
                { text: "Nombre de buts moyen/match\n" },
                {
                  text: (
                    player.playerTiming.but /
                    (player.playerTiming.titulaire +
                      player.playerTiming.nontitulaire)
                  ).toFixed(2),
                  bold: true,
                  color: "#0f81ab",
                  fontSize: 12,
                },
              ],
            ],
            [
              "",
              [
                { text: "Nombre des arrêts décisif\n" },
                {
                  text: player.playerTiming.arret_decisif,
                  bold: true,
                  color: "#0f81ab",
                  fontSize: 12,
                },
              ],
              "",
            ],
          ],
        },
        layout: "noBorders",
      },
      { text: "\n\nCartons:", style: "header" },
      {
        style: "tableExample",
        table: {
          headerRows: 1,
          widths: [400, 400],
          margin: [20],
          body: [
            [
              [
                { text: "cartonJaune\n" },
                {
                  text: player.cartonJaune,
                  bold: true,
                  color: "#0f81ab",
                  fontSize: 12,
                },
              ],
              [
                { text: "cartonRouge\n" },
                {
                  text: player.cartonRouge,
                  bold: true,
                  color: "#0f81ab",
                  fontSize: 12,
                },
              ],
            ],
          ],
        },
        layout: "noBorders",
      },
    ]);
  }
  //! match data
  if (player.matchs.length > 0) {
    if (req.params.stageId != undefined) {
      contents = contents.concat([
        // ! detail du match
        {
          text: "Détail des matchs joués:",
        },
        {
          margin: [6.5, 20, 6.5, 10],
          style: "tbody",
          table: {
            widths: [140, 60, 60, 60, 60, 60, 80],
            body: matchs,
          },
        },
      ]);
      //! match with rapport
      if (matchsWithRapport.length > 0 && Number(req.params.option) == 1) {
        contents = contents.concat([
          {
            text: "Rapport sur les matchs :",
          },
          {
            margin: [6.5, 20, 20, 10],
            style: "tbody",
            table: {
              widths: [120, 390, 40],
              body: matchsWithRapport,
            },
          },
        ]);
      }
    } else {
      contents = contents.concat([
        {
          margin: [6.5, 20, 6.5, 10],
          style: "tbody",
          table: {
            widths: [140, 60, 60, 60, 60, 60, 80],
            body: matchs,
          },
        },
      ]);
      //! match with rapport
      if (matchsWithRapport.length > 0 && Number(req.params.option) == 1) {
        contents = contents.concat([
          {
            text: "Rapport sur les matchs :",
          },
          {
            margin: [6.5, 20, 20, 10],
            style: "tbody",
            table: {
              widths: [120, 390, 40],
              body: matchsWithRapport,
            },
          },
        ]);
      }
    }
  }

  //! entrainement data
  if (player.entrainements.length > 0 && req.params.stageId == undefined)
    contents = contents.concat([
      {
        style: "tbodyE",
        table: {
          widths: [80, 30, 50, 90, 150, 40, 85],
          body: _entrainements,
        },
      },
    ]);

  var ObjectPDF = {
    pageMargins: [0, 0, 0, 0],
    pageSize: "A4",
    content: contents,
    styles: {
      thead: {
        color: "black",
        fillColor: "#08A067",
        bold: true,
        margin: [0, 4],
        padding: 0,
        fontSize: 9,
        alignment: "center",
      },
      icon: { opacity: "1", font: "Roboto" },
      tbody: {
        color: "black",
        fillColor: "white",
        margin: 0,
        padding: 0,
        fontSize: 9,
        alignment: "center",
      },
      tbodyE: {
        color: "black",
        fillColor: "white",
        margin: 0,
        padding: 0,
        fontSize: 7.5,
        alignment: "center",
      },
      tbodyT: {
        color: "black",
        fillColor: "white",
        margin: [0, 10],
        padding: 0,
        fontSize: 9,
        alignment: "center",
      },
      header: {
        fontSize: 10,
        bold: true,
        margin: [0, 0, 0, 10],
      },
      headerE: {
        fontSize: 9,
        bold: true,
        margin: [0, 10, 5, 0],
      },
      title: {
        margin: [0, 20, 0, 10],
        padding: [0, 10, 0, 10],
        bold: true,
        color: "black",
        fontSize: 14,
        alignment: "center",
      },
    },
    footer: (currentPage, pageCount) => {
      if (currentPage == pageCount) {
        return {
          table: {
            widths: "*",
            body: [
              [
                {
                  text:
                    "Date de génération du fichier : " +
                    moment(new Date()).format("YYYY-MM-DD HH:MM:SS"),
                  fontSize: 8,
                  absolutePosition: { x: 10, y: -22 },
                },
              ],
              [
                {
                  text: "Page | " + currentPage.toString(),
                  fontSize: 8,
                  absolutePosition: { x: 535, y: -22 },
                },
              ],
              [
                {
                  image:
                    "data:image/png;base64," +
                    base64_encode("./images/pdf/player/footer_image_pdf.png"),
                  alignment: "center",
                  width: 594,
                  absolutePosition: { x: 0, y: -40 },
                },
              ],
            ],
          },
          layout: "noBorders",
        };
      } else {
        return {
          table: {
            widths: "*",
            body: [
              [
                {
                  text: "Page | " + currentPage.toString(),
                  fontSize: 8,
                  absolutePosition: { x: 535, y: -22 },
                },
              ],
              [
                {
                  image:
                    "data:image/png;base64," +
                    base64_encode("./images/pdf/player/footer_image_pdf.png"),
                  alignment: "center",
                  width: 594,
                  absolutePosition: { x: 0, y: -40 },
                },
              ],
            ],
          },
          layout: "noBorders",
        };
      }
    },
  };
  res.status(200).json(ObjectPDF);
});

const svg = {
  chart: `<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 490 490" style="enable-background:new 0 0 490 490;" xml:space="preserve"> <g id="line_35_"> <path d="M174.203,490V281.061H67.015V490H174.203z M97.64,311.686h45.938v147.689H97.64V311.686z"/> <path d="M204.828,490h107.188V210.623H204.828V490z M235.453,241.248h45.937v218.127h-45.937V241.248z"/> <path d="M449.828,136.312H342.64V490h107.188V136.312z M419.203,459.375h-45.938V166.937h45.938V459.375z"/> <path d="M344.799,0l-89.716,49.046l29.982,19.432C190.572,185.894,41.704,214.344,40.172,214.635l5.451,30.135 c6.845-1.24,163.783-30.962,265.304-159.526l30.855,19.983L344.799,0z"/> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>`,
  table: `<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 445 445" style="enable-background:new 0 0 445 445;" xml:space="preserve"> <path d="M0,37.215v55v15v300.57h445v-300.57v-15v-55H0z M276.667,277.595H168.333v-70.19h108.334V277.595z M306.667,207.405H415 v70.19H306.667V207.405z M276.667,307.595v70.19H168.333v-70.19H276.667z M30,207.405h108.333v70.19H30V207.405z M168.333,177.405 v-70.19h108.334v70.19H168.333z M138.333,107.215v70.19H30v-70.19H138.333z M30,307.595h108.333v70.19H30V307.595z M306.667,377.785 v-70.19H415v70.19H306.667z M415,177.405H306.667v-70.19H415V177.405z"/> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> <g> </g> </svg>`,
};

app.listen(3002, () => {
  console.log("Server listening on port 3002");
});
